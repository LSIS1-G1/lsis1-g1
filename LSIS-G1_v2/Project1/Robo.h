#pragma once
#ifndef Robo_
#define Robo_

#include <string>
using namespace std;

class Robo
{

private:
	string nome;
	string codigo;
	string equipa;
	string cor;

public:
	Robo();
	Robo(const Robo &r);
	Robo(string name, string code, string equi, string color);
	void setNome(string name);
	void setCodigo(string code);
	void setEquipa(string equi);
	void setCor(string color);
	string getNome() const;
	string getCodigo() const;
	string getEquipa() const;
	string getCor() const;
};

Robo::Robo()
{

}
Robo::Robo(const Robo &r)
{
	nome = r.nome;
	codigo = r.codigo;
	equipa = r.equipa;
	cor = r.cor;
}
Robo:: Robo(string name, string code, string equi, string color):
		nome(name), codigo(code), equipa(equi), cor(color)
{

}

void Robo::setNome(string name)
{
	nome = name;
}
void Robo::setCodigo(string code)
{
	codigo = code;
}
void Robo::setEquipa(string equi)
{
	equipa = equi;
}
void Robo::setCor(string color)
{
	cor = color;
}
string Robo::getNome() const
{
	return nome;
}
string Robo::getCodigo() const
{
	return codigo;
}
string Robo::getEquipa() const
{
	return equipa;
}
string Robo::getCor() const
{
	return cor;
}
#endif

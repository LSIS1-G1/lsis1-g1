#pragma once
/**
/* Estas duas instrucoes evitam que a classe seja incluida mais do que uma vez
*/
#ifndef Equipa_
#define Equipa_

#include <string>
using namespace std;

//A classe Equipa tera os atributos correspondentes a tabela Equipa da base de dados
class Equipa
{

private:
	
	string NomeEquipa;
	string nomeElem;
	string lema;
	int idade; //idade dos elemento;
	int numElementos; //numero de elementos da equipa;
	string Localidade;

public:
	Equipa();
	Equipa(const Equipa &eq);
	Equipa(string nomeEq, string nomeEl, string Lema, int age, int numEl, string local);
	void setNomeEq(string nomeEq);
	void setNomeEl(string nomeEle);
	void setLema(string lemaEq);
	void setIdade(int idade);
	void setNumElementos(int numEle);
	void setLocalidade(string local);
	string getNomeEq() const;
	string getNomeEle() const;
	string getLema() const;
	int getIdade() const;
	int getNumElementos() const;
	string getLocalidade() const;
};

Equipa::Equipa()
{
}

Equipa:: Equipa(const Equipa &eq)
{
	NomeEquipa = eq.NomeEquipa;
	nomeElem = eq.nomeElem;
	lema = eq.lema;
	idade = eq.idade;
	numElementos = eq.numElementos;
	Localidade = eq.Localidade;

}

Equipa::Equipa(string nomeEq, string nomeEl, string Lema, int age, int numEl, string local) :
	NomeEquipa(nomeEq), nomeElem(nomeEl), lema(Lema), idade(age), numElementos(numEl), Localidade(local)
{

}

 void Equipa::setNomeEq(string nomeEq)
{
	NomeEquipa = nomeEq;
}

void Equipa:: setNomeEl(string nomeEl)
{
	nomeElem = nomeEl;
}

void Equipa::setIdade(int age)
{
	idade = age;
}

void Equipa::setNumElementos(int numEl)
{
	numElementos = numEl;
}

void Equipa::setLema(string Lema)
{
	lema = Lema;
}

void Equipa::setLocalidade(string local)
{
	Localidade = local;
}

string Equipa::getNomeEq() const
{
	return NomeEquipa;
}
string Equipa::getNomeEle() const
{
	return nomeElem;
}
string Equipa::getLema()const
{
	return lema;
}
int Equipa::getIdade() const
{
	return idade;
}
int Equipa::getNumElementos() const
{
	return numElementos;
}
string Equipa::getLocalidade()const
{
	return Localidade;
}

#endif
#pragma once
#ifndef Prova_
#define Prova_

#include <string>
using namespace std;

class Prova
{
private:
	int data;
	int hora;
	string local;
	string tempo;
	string posicao;
	
public:
	Prova();
	Prova(const Prova &pr);
	Prova(int date, int hour, string localP, string time, string posicionamento);
	void setData(int date);
	void setHora(int hour);
	void setLocal(string localP);
	void setTempo(string time);
	void setPosicao(string posicionamento);
	int getData() const;
	int getHora() const;
	string getLocal() const;
	string getTempo() const;
	string getPosicao() const;

};

Prova::Prova()
{

}
Prova::Prova(const Prova &pr)
{
	data = pr.data;
	hora = pr.hora;
	local = pr.local;
	tempo = pr.tempo;
	posicao = pr.posicao;
}
Prova::Prova(int date, int hour, string localP, string time, string posicionamento) :
	data(date), hora(hour), local(localP), tempo(time), posicao(posicionamento)
{

}
void Prova::setData(int date)
{
	data = date;
}
void Prova::setHora(int hour)
{
	hora = hour;
}
void Prova::setTempo(string time)
{
	tempo = time;
}
void Prova::setLocal(string localP)
{
	local = localP;
}
void Prova::setPosicao(string posicionamento)
{
	posicao = posicionamento;
}

int Prova::getData() const
{
	return data;
}
int Prova::getHora() const
{
	return hora;
}
string Prova::getLocal() const
{
	return local;
}
string Prova::getPosicao() const
{
	return posicao;
}
string Prova::getTempo() const
{
	return tempo;
}

#endif

#pragma once
#ifndef Configuracao_
#define Configuracao_

#include <string>
using namespace std;

class Configuracao
{
private:
	string sensor;
	int velocidade;
	string funcoes;

public:
	Configuracao();
	Configuracao(const Configuracao &con);
	Configuracao(string sensores, int veloc, string functions);
	void setSensor(string sensores);
	void setVelocidade(int veloc);
	void setFuncoes(string functions);
	string getSensor() const;
	int getVelocidade() const;
	string getFuncoes() const;

};

Configuracao::Configuracao()
{

}
Configuracao::Configuracao(const Configuracao &con)
{
	sensor = con.sensor;
	velocidade = con.velocidade;
	funcoes = con.velocidade;
}
Configuracao::Configuracao(string sensores, int veloc, string functions) :
	sensor(sensores), velocidade(veloc), funcoes(functions)
{

}
void Configuracao::setSensor(string sensores)
{
	sensor = sensores;
}
void Configuracao::setVelocidade(int veloc)
{
	velocidade = veloc;
}
void Configuracao::setFuncoes(string functions)
{
	funcoes = functions;
}
string Configuracao::getSensor() const
{
	return sensor;
}
int Configuracao::getVelocidade() const
{
	return velocidade;
}
string Configuracao::getFuncoes() const
{
	return funcoes;
}
#endif

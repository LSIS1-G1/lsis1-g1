#include <iostream>
#include <string>
#include <exception>
#include <stdlib.h>

#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/sqlstring.h>

#include "Equipa.h"

using namespace std;
using namespace sql;


class BaseDados {
private:

	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
	string nomeBD;

public:
	BaseDados();
	///////////////////////~BaseDados();
	BaseDados(string nome);

	void ligar();
	bool lerEquipa();
	void inserir(Equipa &eq);
	void inserirRobo(); //????
	void fechar();
};


BaseDados::BaseDados()
{
	nomeBD = "lsis1_g01";
	ligar();
}

BaseDados::~BaseDados() {
	con->close();
	delete stmt;
	delete con;

}

void BaseDados::ligar() {
	cout << "A ligar...";
	driver = get_driver_instance();
	try {
		con = driver->connect("tcp://127.0.0.1:3306", "Ana@localhost", "password");
		con->setSchema(nomeBD);
		stmt = con->createStatement();
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}
#pragma once
#ifndef Codigo_
#define Codigo_

#include <string>
using namespace std;

class Codigo
{
private:
	string codigo;
	string funcao;

public:
	Codigo();
	Codigo(const Codigo &c);
	Codigo(string code, string function);
	void setCodigo(string code);
	void setFuncao(string function);
	string getCodigo() const;
	string getFuncao() const;

};

Codigo::Codigo() 
{

}

Codigo::Codigo(const Codigo &c)
{
	codigo = c.codigo;
	funcao = c.funcao;

}
Codigo::Codigo(string code, string function):
	  codigo(code),funcao(function)
{
	
}
void Codigo::setCodigo(string code)
{
	codigo = code;
}
void Codigo::setFuncao(string function)
{
	funcao = function;
}
string Codigo:: getCodigo() const
{
	return codigo;
}
string Codigo::getFuncao() const
{
	return funcao;
}
#endif // !Codigo_

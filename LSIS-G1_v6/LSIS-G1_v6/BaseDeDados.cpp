#include "BaseDeDados.h"
#include "utils.h"
#include <string>
using namespace std;

BaseDados::BaseDados()
{
	nomeBD = "v2";
	ligar();
}

BaseDados::~BaseDados() {
	con->close();
	delete stmt;
	delete con;

}

void BaseDados::ligar() {
	cout << "A ligar..."<<endl;
	driver = get_driver_instance();
	try {
		con = driver->connect("tcp://127.0.0.1:3306", "root", "");
		con->setSchema(nomeBD);
		stmt = con->createStatement();
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}
void BaseDados::fechar()
{
	BaseDados::~BaseDados();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////VERIFICA SE A EQUIPA EXISTE/////////////////////////////////////// CHECK!
bool BaseDados::lerEquipa(int numEqui) {
	strSQL = stmt->executeQuery("SELECT NumEquipa FROM equipas WHERE NumEquipa = " + numberToString(numEqui));
	int numLinhas = 0;
	while (strSQL->next())
	{
		numLinhas++;
	}
	if (numLinhas >= 1)
	{
		cout << "Equipa existente!" << endl;
		return true;
	}
	else
	{
		//cout << "Equipa nao existe!" << endl;
		return false;
	}
}

/////////////////////////////////////METODOS PARA A TABELA EQUIPA////////////////////////////////////////////

/////////////////////////////////MOSTRAR APENAS UMA EQUIPA/////////////////////////////// CHECK!
void BaseDados::mostrarEquipa(int numEquipa)
{
	try
	{
		res = stmt->executeQuery("SELECT Nome, NumElementos, Lema, Localidade, NumEquipa from equipas where NumEquipa = " + numberToString(numEquipa));
		while (res->next())
		{
			string nome = res->getString("Nome");
			int elementos = res->getInt("NumElementos");
			string lema = res->getString("Lema");
			string local = res->getString("Localidade");
			cout << "Equipa: " << nome << " ; " << elementos << " ; " << lema << " ; " << local << endl;
		}
		cout << "Operacao realizada com sucesso!" << endl;
	}

	catch (SQLException &e)
	{
		cout << /*"#SQL Exception: " <<*/ e.what() << endl;
		//cout << " MySQL error code: " << e.getErrorCode();
		//cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}
}

/////////////////////////////////ATUALIZAR UMA EQUIPA///////////////////////////////////// CHECK!
void BaseDados::atualizarEquipas(int numEquipa)
{
	int opc = 0, newNumEl;
	string newName, newLema, newLocal;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome da equipa" << endl << "2) Numero de elementos" << endl << "3) Lema da equipa" << endl << "4) Localidade da equipa" << endl <<"5 )Voltar a INFORMACAO DA EQUIPA" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar nome da equipa
			cout << "Introduza o novo nome da equipa: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("UPDATE equipas SET Nome = '"+newName+"' WHERE NumEquipa = "+ numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!"<<endl;

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar numero de elementos
			cout << "Introduza o novo numero de elementos na equipa: " << endl;
			cin >> newNumEl;
			try {
				stmt->executeUpdate("UPDATE equipas SET NumElementos = '" + numberToString(newNumEl) + "'where NumEquipa = " + numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!"<<endl;

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar lema da equipa
			cout << "Introduza o novo lema da equipa: " << endl;
			cin >> newLema;

			try {
				stmt->executeUpdate("UPDATE equipas SET Lema = '" + newLema + "' WHERE NumEquipa = " + numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!"<<endl;

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //alterar localidade da equipa
			cout << "Introduza a nova localidade da equipa: " << endl;
			cin >> newLocal;
			try {
				stmt->executeUpdate("UPDATE equipas SET Localidade = '" + newLocal+ "' WHERE NumEquipa = " + numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!"<<endl;

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: //volta ao menu informacao da equipa
			break;
		default: 
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;
		}
	} while (opc != 5);
}

////////////////////////////////LISTAR EQUIPAS//////////////////////////////////////////////// CHECK!
void BaseDados::listarEquipas()
{
	try
	{
		res = stmt->executeQuery(" SELECT Nome, NumElementos, Lema, Localidade, NumEquipa from  equipas ");
		while (res->next()) 
		{
			string nome = res->getString("Nome");
			int elementos = res->getInt("NumElementos");
			string lema = res->getString("Lema");
			string local = res->getString("Localidade");
			int numero = res->getInt("NumEquipa");
			cout << "Equipa: " << numero << " ; " << nome << " ; " << elementos << " ; " << lema << " ; " << local << endl;
		}
		//cout << res << endl; //para listar 
		cout << "Operacao realizada com sucesso!" << endl;
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////INSERIR EQUIPAS////////////////////////////////////////////////
void BaseDados::inserirEquipa(Equipa &eq) 
{
	try
	{
		//stmt->execute("insert into equipas(Nome, NumElementos, Lema, Localidade, NumEquipa)values('" +eq.getNomeEq()+"','" + numberToString(eq.getNumElementos()) + "','"+  eq.getLema() + "','" + eq.getLocalidade() + "','"+numberToString(eq.getNumEquipa())+"')");
		cout << "'" + eq.getNomeEq()  +  "', " + numberToString(eq.getNumElementos()) + ", '" + eq.getLema() + "', '" + eq.getLocalidade() + "', " + numberToString(eq.getNumEquipa())  << endl;
		string inst = "INSERT INTO equipas (Nome, NumElementos, Lema, Localidade, NumEquipa) VALUES('" + eq.getNomeEq()  +  "', " + numberToString(eq.getNumElementos()) + " , '" + eq.getLema() + "', '" + eq.getLocalidade() + "', " + numberToString(eq.getNumEquipa()) + ")";
		stmt->execute(inst);
			
			cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

///////////////////////APAGAR UMA EQUIPA NA TABELA equipas/////////////////////////////////
void BaseDados::eliminarEquipa(int numEquipa)
{
	try
	{
		stmt->executeUpdate("DELETE from equipas where NumEquipa = " + numberToString(numEquipa));
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////ROBO//////////////////////////////////////////////////////////////////////////

//////////////////////////LER ROBO/////////////////////////////////////////////////////////////
bool BaseDados::lerRobo(string codeRobo)
{
	strSQL = stmt->executeQuery("SELECT Codigo FROM robo WHERE Codigo = " + codeRobo);
	int numLinhas = 0;
	while (strSQL->next())
	{
		numLinhas++;
	}
	if (numLinhas >= 1)
	{
		cout << "Robo existente!" << endl;
		return true;
	}
	else
	{
		//cout << "Robo nao existe!" << endl;
		return false;
	}
}

////////////////////////////////LISTAR ROBOS///////////////////////////////////////////
void BaseDados::listarRobo()
{
	try
	{
		res = stmt->executeQuery("SELECT Nome, Codigo, Equipa, Cor, FuncaoP FROM robo ");
		while (res->next())
		{
			string nome = res->getString("Nome");
			string codigo = res->getString("Codigo");
			int equipa = res->getInt("Equipa");
			string cor = res->getString("Cor");
			string funcaoP = res->getString("FuncaoP");
			cout << "Robo: " << codigo << " ; " << nome << " ; " << equipa << " ; " << cor << " ; " << funcaoP << endl;
		}
		cout << "Operacao realizada com sucesso!" << endl;
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}

////////////////////////////////MOSTRAR UM ROBO //////////////////////////////////////
void BaseDados::mostrarRobo(string codigoR)
{
	try
	{
		res = stmt->executeQuery("SELECT Nome, Codigo, Equipa, Cor, FuncaoP FROM robo where Codigo = " + codigoR);
		while (res->next())
		{
			string nome = res->getString("Nome");
			string codigo = res->getString("Codigo");
			int equipa = res->getInt("Equipa");
			string cor = res->getString("Cor");
			string funcaoP = res->getString("FuncaoP");
			cout << "Robo: " << codigo << " ; " << nome << " ; " << equipa << " ; " << cor << " ; " << funcaoP << endl;
		}
		cout << "Operacao realizada com sucesso!" << endl;
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

/////////////////////////////////ATUALIZAR INFORMACAO DO ROBO////////////////////////////
void BaseDados::atualizarRobo(string codeR)
{

	int opc = 0, newTeam=0;
	string newName, newColour, newFunction;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome do robo" << endl << "2) Equipa do robo" << endl << "3) Cor do robo" << endl << "4) Funcao principal" <<  endl << "5) Voltar a INFORMACAO DO ROBO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: 
			//alterar nome do robo 
			cout << "Introduza o novo nome do robo: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("UPDATE robo SET Nome = '" + newName + "' WHERE Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 2: 
			//alterar a equipa do robo
			cout << "Introduza o numero da nova equipa: " << endl;
			cin >> newTeam;
			try {
				stmt->executeUpdate("UPDATE robo SET  NumEquipa = " + numberToString(newTeam) + " where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: 
			//alterar cor do robo
			cout << "Introduza a nova cor do robo: " << endl;
			cin >> newColour;

			try {
				stmt->executeUpdate("UPDATE robo SET Cor = '" + newColour + "' where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: 
			//alterar funcao principal do robo
			cout << "Introduza a nova funcao principal do robo: " << endl;
			cin >> newFunction;
			try {
				stmt->executeUpdate("UPDATE robo set FuncaoP = '" + newFunction + "' where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: 
			//voltar ao menu INFORMACAO DO ROBO
			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;
		}
	} while (opc != 5);
}

////////////////////////////////INSERIR NA TABELA rob�////////////////////////////////
void BaseDados:: inserirRobo (Robo &ro)
{
	try
	{
		string inst = "INSERT INTO robo (Nome, Codigo, Equipa, Cor, FuncaoP) VALUES('" + ro.getNome() + "', '" + ro.getCodigo() + "' , " + numberToString(ro.getNumeroEquipa()) + ", '" + ro.getCor() + "', '" + ro.getFuncaoP() + "')";
		stmt->execute(inst);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////ELIMINAR NA TABELA rob�/////////////////////////////////
void BaseDados::eliminarRobo(string codeRobot)
{
	try
	{
		stmt->executeUpdate("DELETE from robo where Codigo = " + codeRobot);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////PROVAS//////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////LER PROVA//////////////////////////////////////////////////////
bool BaseDados::lerProva(string codeProva)
{
	strSQL = stmt->executeQuery("SELECT Codigo FROM provas WHERE Codigo = " + codeProva);
	int numLinhas = 0;
	while (strSQL->next())
	{
		numLinhas++;
	}
	if (numLinhas >= 1)
	{
		cout << "Prova existente!" << endl;
		return true;
	}
	else
	{
		//cout << "Prova nao existe!" << endl;
		return false;
	}
}

////////////////////////////////////ATUALIZAR PROVAS///////////////////////////////////////////////
void BaseDados::atualizarProva(string codePro)
{

	int opc = 0, newTime, newTeam;
	string  newDate, newHour, newLocal, newPos;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Equipa" << endl << "2) Data da prova" << endl << "3) Hora da prova" << endl << "4) Local da prova" << endl <<"5) Tempo da prova"<<endl << "6) Posicionamento"<< endl << "7) Voltar a INFORMACAO DA PROVA" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar equipa
			cout << "Introduza o numero da nova equipa: " << endl;
			cin >> newTeam;
			try {
				stmt->executeUpdate("UPDATE provas SET NumEquipa = " + numberToString(newTeam)+ "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a data da prova
			cout << "Introduza a nova data: " << endl;
			cin >> newDate;
			try 
			{
				stmt->executeUpdate("UPDATE provas SET  DataProva = '" + newDate + "' where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar a hora da prova
			cout << "Introduza a nova hora da prova: " << endl;
			cin >> newHour;
			try
			{
				stmt->executeUpdate("UPDATE provas SET HoraProva = '" + newHour + "' where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //alterar o local da prova
			cout << "Introduza o novo local da prova: " << endl;
			cin >> newLocal;
			try 
			{
				stmt->executeUpdate("UPDATE provas SET LocalProva = '" + newLocal + "' where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: //alterar o tempo de prova
			cout << "Introduza o novo tempo de duracao da prova: " << endl;
			cin >> newTime;
			try
			{
				stmt->executeUpdate("UPDATE provas SET TempoProva = " + numberToString(newTime) + "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Operacao realizada com sucesso!";
			}

			break;
		case 6: //alterar o posicionamento da equipa na prova
			cout << "Introduza a nova posicao da equipa nas provas";
			cin >> newPos;

			try
			{
				stmt->executeUpdate("UPDATE provas SET Posicionamento = "+ numberToString(newPos) + "where Codigo = " + codePro);
			}
			catch (SQLException &e)
			{
				cout << "Operacao realizada com sucesso!";
			}

			break;
		case 7: //volta ao menu informacao da equipa
			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;
		}
	} while (opc !=7 );
}
///////////////////////////////////LISTAR PROVAS///////////////////////////////////////////////////
void BaseDados::listarProvas()
{
	try
	{
		res = stmt->executeQuery("SELECT Codigo, NumEquipa, DataProva, HoraProva, LocalProva, TempoProva, PosicionamentoProva FROM provas");
		while (res->next())
		{
			string codigo = res->getString("Codigo");
			int NumEquipa = res->getInt("NumEquipa");
			string DataProva = res->getString("DataProva");
			string HoraProva = res->getString("HoraProva");
			string LocalProva = res->getString("LocalProva");
			int TempoProva = res->getInt("TempoProva");
			int PosicionamentoProva = res->getInt("PosicionamentoProva");
			cout << "Prova: " << codigo << " ; " << NumEquipa << " ; " << DataProva << " ; " << HoraProva << " ; " << LocalProva << ";" << TempoProva << ";" << PosicionamentoProva << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////MOSTRAR PROVA//////////////////////////////////////////////////
void BaseDados::mostrarProva(string code)
{
	try
	{
		res = stmt->executeQuery("SELECT Codigo, NumEquipa, DataProva, HoraProva, LocalProva, TempoProva, PosicionamentoProva FROM provas where Codigo = " + code);
		while (res->next())
		{
			string codigo = res->getString("Codigo");
			int NumEquipa = res->getInt("NumEquipa");
			string DataProva = res->getString("DataProva");
			string HoraProva = res->getString("HoraProva");
			string LocalProva = res->getString("LocalProva");
			int TempoProva = res->getInt("TempoProva");
			int PosicionamentoProva = res->getInt("PosicionamentoProva");
			cout << "Prova: " << codigo << " ; " << NumEquipa << " ; " << DataProva << " ; " << HoraProva << " ; " << LocalProva << ";" << TempoProva << ";" << PosicionamentoProva << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}
///////////////////////////////////INSERIR NA TABELA provas////////////////////////////////////////
void BaseDados::inserirProva(Prova &pr)
{
	try
	{
		string inst = "INSERT INTO provas (Codigo, NumEquipa, DataProva, HoraProva, LocalProva, TempoProva, PosicionamentoProva) VALUES('" + pr.getCodigo() + "'," + numberToString(pr.getNumeroEquipa()) + ",'" + pr.getData() + "','" + pr.getHora() + "','" + pr.getLocal() + "'," + numberToString(pr.getTempos()) + "," + numberToString(pr.getPosicao()) + ")";
		stmt->execute(inst);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;

	}	
}

////////////////////////////////////ELIMINAR NA TABELA provas/////////////////////////////////////////
void BaseDados::eliminarProva(string codigo )
{
	try
	{
		stmt->executeUpdate("DELETE from provas where Codigo = " + codigo);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////JOGADORES//////////////////////////////////////////////////

//////////////////////////////////////////LER JOGADOR//////////////////////////////////////////////////
bool BaseDados::lerJogador(int numJog)
{
	strSQL = stmt->executeQuery("SELECT NumJogador FROM jogadores where NumJogador = " + numJog);
	int numLinhas = 0;
	while (strSQL->next())
	{
		numLinhas++; 
	}
	if (numLinhas >= 1)
	{
		cout << "Jogador existente!" << endl;
		return true;
	}
	else
	{
		//cout << "Jogador nao existe!" << endl;
		return false;
	}
}

////////////////////////////////////////INSERIR NA TABELA jogadores /////////////////////////////////
void BaseDados::inserirJogador(Jogador &jog)
{
	try
	{
		string inst = "INSERT INTO jogadores (NomeJogador, Idade, NumEquipa, NumJogador) VALUES('" + jog.getNomeJogador() + "' , " + numberToString(jog.getIdadeJogador()) + "," + numberToString(jog.getNumeroEquipa()) + "," + numberToString(jog.getNumeroJogador()) + ")";
		stmt->execute(inst);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////////ELIMINAR NA TABELA jogadores////////////////////////////////
void BaseDados::eliminarJogador(int numJog)
{
	try
	{
		stmt->executeUpdate("DELETE from jogadores where NumJogador = " + numJog);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}
//////////////////////////////////////////MOSTRAR jogador/////////////////////////////////////////////
void BaseDados::mostrarJogador(int jog)
{
	try
	{
		res = stmt->executeQuery("SELECT NomeJogador,Idade,NumEquipa,NumJogador FROM jogadores where NumJogador = " +jog);
		while (res->next())
		{
			string NomeJogador = res->getString("NomeJogador");
			int Idade = res->getInt("Idade");
			int NumEquipa = res->getInt("NumEquipa");
			int codigo = res->getInt("NumJogador");

			cout << "Jogadores: " << codigo << " ; " << NomeJogador << " ; " << Idade << " ; " << NumEquipa << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}

//////////////////////////////////////////LISTAR JOGADORES////////////////////////////////////////////
void BaseDados::listarJogadores()
{
	try
	{
		res = stmt->executeQuery("SELECT NomeJogador,Idade,NumEquipa,NumJogador FROM jogadores");
		while (res->next())
		{
			string NomeJogador = res->getString("NomeJogador");
			int Idade = res->getInt("Idade");
			int NumEquipa = res->getInt("NumEquipa");
			int codigo = res->getInt("NumJogador");

			cout << "Jogadores: " << codigo << " ; " << NomeJogador << " ; " << Idade << " ; " << NumEquipa << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////////////////////////////////////////////ATUALIZAR JOGADOR////////////////////////////////////////////////////
void BaseDados::atualizarJogador(int numJog)
{

	int opc = 0, newAge = 0, numTeam = 0;
	string newName;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome do jogador" << endl << "2) Idade do jogador" << endl << "3) Numero de equipa do jogador" << endl << "4) Voltar a INFORMACAO DO ROBO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar nome do jogador
			cout << "Introduza o novo nome do jogador: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("UPDATE jogadores SET NomeJogador = '" + newName + "' where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a idade do jogador
			cout << "Introduza a nova idade do jogador: " << endl;
			cin >> newAge;
			try {
				stmt->executeUpdate("UPDATE jogadores SET  Idade = " + numberToString(newAge) + "where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar o numero da equipa do jogador
			cout << "Introduza o novo numero da equipa do jogador: " << endl;
			cin >> numTeam;
			try {
				stmt->executeUpdate("UPDATE jogadores SET NumEquipa = " + numberToString(numTeam) + "where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //voltar ao menu INFORMACAO DO JOGADOR
			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;
		}
	} while (opc != 4);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////CONFIGURACAO//////////////////////////////////////////////////////

///////////////////////////////////////LER CONFIGURACOES/////////////////////////////////////////////
bool BaseDados::lerConfiguracao(string code)
{
	strSQL = stmt->executeQuery("SELECT Codigo FROM configuracaorobo where Codigo = " + code);
	int numLinhas = 0;
	while (strSQL->next())
	{
		numLinhas++;
	}
	if (numLinhas >= 1)
	{
		cout << "Configuracao existente!" << endl;
		return true;
	}
	else
	{
		//cout << "Config. nao existe!" << endl;
		return false;
	}
}

///////////////////////////////////////ATUALIZAR CONFIGURACOES///////////////////////////////////////
void BaseDados::atualizarConfiguracao(string codeConf)
{

	int opc = 0;
	string newSensor, newVeloc, newFunctions;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Tipo de sensores" << endl << "2) Velocidade maxima" << endl << "3) Funcoes extras"  << endl << "4) Voltar a INFORMACAO DA CONFIGURACAO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar os tipos de sensores 
			cout << "Introduza os novos tipos de sensores: " << endl;
			cin >> newSensor;
			try {
				stmt->executeUpdate("UPDATE configuracaorobo SET Sensores = '" + newSensor + "' where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a velocidade maxima 
			cout << "Introduza a nova velocidade maxima: " << endl;
			cin >> newVeloc;
			try {
				stmt->executeUpdate("UPDATE configuracaorobo SET  Velocidade = " + numberToString(newVeloc) + "where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar as funcoes extras 
			cout << "Introduza as novas funcoes do robo: " << endl;
			cin >> newFunctions;

			try {
				stmt->executeUpdate("UPDATE configuracaorobo SET Funcoes = '" + newFunctions + "' where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		
	
		case 4: //volta ao menu informacao das configuracoes
			break;

		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;


		}
	} while (opc != 4);
}


///////////////////////////////////////LISTAR CONFIGURACOES//////////////////////////////////////////
void BaseDados::listarConfiguracao()
{
	try
	{
		res = stmt->executeQuery("SELECT Sensores, Velocidade, Funcoes, Codigo FROM configuracaorobo");
		while (res->next())
		{
			string Sensores = res->getString("Sensores");
			int Velocidade = res->getInt("Velocidade");
			string Funcoes = res->getString("Funcoes");
			string codigo = res->getString("Codigo");
			cout << "Configuracao: " << codigo << " ; " << Sensores << " ; " << Velocidade << " ; " << Funcoes << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

///////////////////////////////////////MOSTRAR DETERMINADAS CONFIGURACOES////////////////////////////////////////
void BaseDados::mostrarConfiguracao(string code)
{
	try
	{
		res = stmt->executeQuery("SELECT Sensores, Velocidade, Funcoes, Codigo FROM configuracaorobo where Codigo = " + code);
		while (res->next())
		{
			string Sensores = res->getString("Sensores");
			int Velocidade = res->getInt("Velocidade");
			string Funcoes = res->getString("Funcoes");
			string codigo = res->getString("Codigo");
			cout << "Configuracao: " << codigo << " ; " << Sensores << " ; " << Velocidade << " ; " << Funcoes << endl;
		}
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}


//////////////////////////////////////INSERIR NA TABELA CONFIGS////////////////////////////////
void BaseDados::inserirConfiguracao(Configuracao &confi)
{
	try
	{
		string inst = "INSERT INTO configuracaorobo (Codigo, Sensores, Velocidade, Funcoes) VALUES('" + confi.getCodigo() + "' , '" + confi.getSensor() + "' , " + numberToString(confi.getVelocidade()) + ",'" + confi.getFuncoes() + "')";
		stmt->execute(inst);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////////////////////////////////////ELIMINAR NA TABELA CONFIGS////////////////////////////////
void BaseDados::eliminarConfiguracao(string codigo)
{
	try
	{
		stmt->executeUpdate("DELETE from configuracaorobo where Codigo = " + codigo);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}



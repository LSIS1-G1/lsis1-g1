#include "Robo.h"
#include <iostream>

Robo::Robo()
{

}
Robo::Robo(const Robo &r)
{
	nome = r.nome;
	codigo = r.codigo;
	numEquipa = r.numEquipa;
	cor = r.cor;
	funcaoP = r.funcaoP;

}
Robo::Robo(string name, string code, int numTeam, string color, string functionP) :
	nome(name), codigo(code), numEquipa(numTeam), cor(color), funcaoP(functionP)
{

}

void Robo::setNome(string name)
{
	nome = name;
}
void Robo::setCodigo(string code)
{
	codigo = code;
}
void Robo::setNumeroEquipa(int numTeam)
{
	numEquipa = numTeam;
}
void Robo::setCor(string color)
{
	cor = color;
}
void Robo::setFuncaoP(string functionP)
{
	funcaoP = functionP;
}
string Robo::getNome() const
{
	return nome;
}
string Robo::getCodigo() const
{
	return codigo;
}
int Robo::getNumeroEquipa() const
{
	return numEquipa;
}
string Robo::getCor() const
{
	return cor;
}
string Robo::getFuncaoP() const
{
	return funcaoP;
}

void Robo::preencherRobo(string codeRobo)
{
	Robo *robot = new Robo();

	cout << "Introduza o nome do robo: " << endl;
	cin >> nome;

	cout << "Introduza o numero da equipa do robo" << endl;
	cin >> numEquipa;

	cout << "Introduza a cor do robo: " << endl;
	cin >> cor;

	cout << "Introduza a funcao principal do robo: " << endl;
	cin >> funcaoP;
	
	codigo = codeRobo;
	
	//robot = new Robo(nomeR, codeRobo, numE, colour, funP);
}

#include <iostream>
#include <string>
//#include <ctype.h>

using namespace std;

#include "Menu.h"
#include "Equipa.h"
#include "BaseDeDados.h"
#include "Robo.h"
#include "utils.h"
#include "Configuracao.h"
#include "Jogador.h"


char Menu::lerOpcao() {
	char opcao = '0';
	cin >> opcao;
	return opcao;
}

void Menu::menuPrincipal()
{
	char opc;
	//bool programa = true;
	do
	{
		cout << endl;
		cout << "******************** MENU PRINCIPAL ****************" << endl;
		cout << "1) Equipa" << endl << "2) Robo" << endl << "3) Prova" << endl << "4) Jogadores" << endl << "5) Configuracao" << endl << "6) Creditos" << endl << "7) Encerrar o programa" << endl << "****************************************************" << endl;
		opc = lerOpcao();

		switch (opc)
		{  
		case '1':
			//inserir menu equipa
			menuEquipa();
			break;
		case '2':
			//inserir menu robo
			menuRobo();
			break;
		case '3':
			//inserir menu prova
			menuProva();
			break;
		case '4':
			//inserir menu jogadores
			menuJogadores();
			break;
		case '5':
			//inserir menu configuracao
			menuConfiguracao();
			break;
		case '6':
			cout << "****************************************************" << endl;
			cout << "*********************** CREDITOS *******************" << endl;
			cout << "No ambito da disciplina Laboratorios de Sistemas 1, da Licenciatura em Engenharia de Sistemas, foi proposto a realizacao de um robo bombeiro e de um sistema de informacao para gerir toda a informacao para uma competicao deste tipo." << endl;
			cout << "Docentes: " << endl << "Andre Dias" << endl << "Alberto Sampaio" << endl;
			cout << "2015/2016" << endl;
			cout << "Trabalho realizado por: " << endl << "Ana Nogueira - 1140476" << endl << "Daniel Barbosa - 1141251" << endl << "Diogo Monteiro - 1140544" << endl << "Pedro Ribeiro - 1141188" << endl;
			cout << "****************************************************" << endl;
			break;
		case '7': //Encerramento do programa
			//cout << "Programa terminado." << endl;
			// programa = false;
			//return;
			exit(1);
			break;

		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. " << endl;
			break;
		}
	} while (opc != 7);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuEquipa()
{
	char opcEquipa;
	int numEquipa = 1, nEquipa = 0;
	string NomeEquipa, lema, Localidade;
	char resp;
	Equipa* equi = new Equipa();
	BaseDados* bd = new BaseDados();
	do {

		cout << endl << "********************MENU EQUIPA****************" << endl;
		cout << "1) Informacao equipa" << endl << "2) Inserir equipa" << endl << "3) Eliminar equipa" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
		opcEquipa = lerOpcao();
		char opcEq;
		switch (opcEquipa)
		{
		case '1':
			do
			{
				int codeTeam;
				cout << "***************INFORMACAO DA EQUIPA***************" << endl;
				cout << "1) Listar equipas " << endl << "2) Visualizar uma equipa" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu equipa" << endl << "***********************************************" << endl;
				opcEq = lerOpcao();
				switch (opcEq)
				{
				case '1':
					//listar equipas
					bd->listarEquipas();
					break;
				case '2': 
					//visualizar toda a informacao da equipa
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeTeam;
					if ((bd->lerEquipa(codeTeam)) == true)
					{
						bd->mostrarEquipa(codeTeam);
					}
					else
					{			
						while ((bd->lerEquipa(codeTeam)) == false) 
						{
								cout << "Insira o numero da equipa que pretende visualizar: ";
								cin >> codeTeam;
							
						} 
					}
					break;
				case '3': 
					//atualizar informacao
					//primeiro tem de saber qual a equipa que pretende
					cout << "Introduza o numero da equipa que pretende alterar: " << endl;
					cin >> codeTeam;
					if ((bd->lerEquipa(codeTeam)) == true)
					{
						bd->atualizarEquipas(codeTeam);
					}
					else
					{
						while ((bd->lerEquipa(codeTeam)) == false)
						{
							cout << "Insira o numero da equipa que pretende alterar: ";
							cin >> codeTeam;

						}
					}
					break;
				case '4':
					//retorna ao menu equipa
					menuEquipa();
					break;
				case '0':
				default:
					cout << "Opcao invalida! Insira novamente uma opcao viavel. " << endl;
					break;
				}

			} while (opcEq != 4);

			break;
		case '2':
			//inserir equipa (base de dados)
				cout << "Introduza o numero da equipa: " << endl;
				cin >> nEquipa;
				if ((bd->lerEquipa(nEquipa)) == true)
				{
					while ((bd->lerEquipa(nEquipa)) == true)
					{
						cout << "Insira outro n�mero: ";
						cin >> nEquipa;
					}
				}
				else
				{
					equi->preencherEquipa(nEquipa);
					bd->inserirEquipa(*equi);
				}
			break;
		case '3':
				//eliminar equipa (base de dados)
				cout << "Indique o numero de equipa que pretende eliminar: ";
				cin >> nEquipa;
				bd->eliminarEquipa(nEquipa);
			break;
		case '4':
				//retornar ao menu principal;
				menuPrincipal();
				break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcEquipa != 4);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuRobo()
{
	char opcRobo;
	string codeRobo;
	char resp;
	Robo *robot = new Robo();
	BaseDados *bd = new BaseDados();

	do {
		cout << endl << "********************MENU ROBO****************" << endl;
		cout << "1) Informacao robo" << endl << "2) Inserir robo" << endl << "3) Eliminar robo" << endl << "4) Retornar menu principal" << endl << "*********************************************" << endl;
		opcRobo = lerOpcao();
		switch (opcRobo)
		{
		case '1': 
			//Informacao Robo
			char opcR;
			do
			{
				string codeRobot;
				cout << "***************INFORMACAO DO ROBO***************" << endl;
				cout << "1) Listar robos " << endl << "2) Visualizar um robo" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu robo" << endl << "*********************************************" << endl;
				opcR = lerOpcao();
				switch (opcR)
				{
				case '1': 
					//Listar Robos
					bd->listarRobo();
					break;

				case '2': 
					//Visualizar um robo
					cout << "Introduza o codigo do robo que pretende visualizar: " << endl;
					cin >> codeRobot;
					if ((bd->lerRobo(codeRobot)) == true)
					{
						bd->mostrarRobo(codeRobot);
					}
					else
					{
						while ((bd->lerRobo(codeRobot)) == false)
						{
							cout << "Insira o numero da equipa que pretende visualizar: ";
							cin >> codeRobot;
						}
					}
					break;

				case '3': 
					//Atualizar informacao
					//primeiro e necessario saber qual o robo
					cout << "Introduza o codigo do robo que pretende atualizar informacao: " << endl;
					cin >> codeRobot;
					if ((bd->lerRobo(codeRobot)) == true)
					{
						bd->atualizarRobo(codeRobot);
					}
					else
					{
						while ((bd->lerRobo(codeRobot)) == false)
						{
							cout << "Insira o numero do robo que pretende atualizar: ";
							cin >> codeRobot;
						}
					}
					break;
				case '4': //Retornar ao menu robo
					menuRobo();
					break;
				case '0':
				default:
					cout << "Opcao invalida! Insira novamente a opcao. ";
					break;
				}

			} while (opcR != 4);

			break;
		case '2':
			//inserir robo (base de dados)
			cout << "Introduza o codigo do robo: " << endl;
			cin >> codeRobo;
			if ((bd->lerRobo(codeRobo)) == true)
			{
				while ((bd->lerRobo(codeRobo)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codeRobo;
				}
			}
			else
			{
				robot->preencherRobo(codeRobo);
				bd->inserirRobo(*robot);
			}
			break;
		case '3':
			//eliminar robo (base de dados)
			cout << "Indique o numero do robo que pretende eliminar: ";
			cin >> codeRobo;
			bd->eliminarRobo(codeRobo);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0': 
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcRobo != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuProva()
{
	BaseDados *bd = new BaseDados();
	Prova *prova = new Prova();
	string codProva;
	char resp;
	char opcProv;
	do
	{
		cout << endl << "********************MENU PROVA****************" << endl;
		cout << "1) Informacao prova" << endl << "2) Inserir prova" << endl << "3) Eliminar prova" << endl << "4) Retornar menu principal" << endl << "**********************************************" << endl;
		opcProv = lerOpcao();
		char opcP;
		switch (opcProv)
		{
		case '1':
			//lerProva da base de dados
			do
			{
				string codeProva;
				cout << endl << "***************INFORMACAO DA PROVA***************" << endl;
				cout << "1) Listar provas " << endl << "2) Visualizar informacao de uma prova" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu provas" << endl << "*********************************************" << endl;
				opcP = lerOpcao();
				switch (opcP)
				{
				case '1': //Listar Prova
					bd->listarProvas();
					break;

				case '2': //Visualizar uma prova
					cout << "Introduza o codigo da prova que pretende visualizar: " << endl;
					cin >> codeProva;
					if ((bd->lerProva(codeProva)) == true)
					{
						bd->mostrarProva(codeProva);
					}
					else
					{
						while ((bd->lerProva(codeProva)) == false)
						{
							cout << "Insira o numero da equipa que pretende visualizar: ";
							cin >> codeProva;
						}
					}
					break;

				case '3': 
					//Atualizar informacao
				    //primeiro e necessario saber qual a prova que se pretende atualizar
					cout << "Introduza o codigo da prova que pretende atualizar informacao: " << endl;
					cin >> codeProva;
					if ((bd->lerProva(codeProva)) == true)
					{
						bd->atualizarProva(codeProva);
					}
					else
					{
						while ((bd->lerProva(codeProva)) == false)
						{
							cout << "Insira o numero da prova que pretende atualizar: ";
							cin >> codeProva;
						}
					}
					break;
				case '4': //torna ao menu prova
					menuProva();
					break;
				case '0':
				default:
					cout << "Opcao invalida! Insira novamente a opcao. ";
					break;
				}
			} while (opcP != 4);

			break;
		case '2':
			//inserir prova (base de dados)
			cout << "Introduza o codigo da prova: " << endl;
			cin >> codProva;
			if ((bd->lerProva(codProva)) == true)
			{
				while ((bd->lerProva(codProva)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codProva;
				}
			}
			else
			{
				prova->preencherProva(codProva);
				bd->inserirProva(*prova);
			}
			break;
		case '3':
			//eliminar prova (base de dados)
			cout << "Indique o numero da prova que pretende eliminar: ";
			cin >> codProva;
			bd->eliminarProva(codProva);
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcProv != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void Menu::menuJogadores()
{
	BaseDados *bd = new BaseDados();
	Jogador *jog = new Jogador();
	int numJog;
	char opcM;
	char resp;
	do
	{
		cout << endl << "********************MENU JOGADOR****************" << endl;
		cout << "1) Informacao do jogador" << endl << "2) Inserir jogador" << endl << "3) Eliminar jogador" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
		opcM = lerOpcao();
		char opcJo;
		switch (opcM)
		{
		case '1':
			//lerJogador da base de dados
			do
			{
				int numJ;
				cout << endl << "***************INFORMACAO DO JOGADOR***************" << endl;
				cout << "1) Listar jogador " << endl << "2) Visualizar informacao de uma jogador" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu jogador" << endl << "*********************************************" << endl;
				opcJo = lerOpcao();
				switch (opcJo)
				{
				case '1': //Listar jogadores
					bd->listarJogadores();
					break;

				case '2': //Visualizar um jogador
					cout << "Introduza o numero do jogador que pretende visualizar: " << endl;
					cin >> numJ;
					if ((bd->lerJogador(numJ)) == true)
					{
						bd->mostrarJogador(numJ);
					}
					else
					{
						while ((bd->lerJogador(numJ)) == false)
						{
							cout << "Insira o numero da equipa que pretende visualizar: ";
							cin >> numJ;
						}
					}
					bd->mostrarJogador(numJ);
					break;

				case '3': //Atualizar informacao
						//primeiro e necessario saber qual a prova que se pretende atualizar
					cout << "Introduza o numero do jogador que pretende atualizar informacao: " << endl;
					cin >> numJ;
					if ((bd->lerJogador(numJ)) == true)
					{
						bd->atualizarJogador(numJ);
					}
					else
					{
						while ((bd->lerJogador(numJ)) == false)
						{
							cout << "Insira o numero da equipa que pretende atualizar: ";
							cin >> numJ;
						}
					}
					break;

				case '4': //torna ao menu jogadores
					menuJogadores();
					break;
				case '0':
				default:
					cout << "Opcao invalida! Insira novamente a opcao. ";
					break;;
				}
			} while (opcJo != 4);
			break;

		case '2':
			//inserir jogador (base de dados)
			cout << "Introduza o numero do jogador: " << endl;
			cin >> numJog;
			if ((bd->lerJogador(numJog)) == true)
			{
				while ((bd->lerJogador(numJog)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> numJog;
				}
			}
			else
			{
				jog->preencherJogador(numJog);
				bd->inserirJogador(*jog);
			}
			
			break;
		case '3':
			//eliminar jogador robo (base de dados)
				cout << "Introduza o numero do jogador que pretende eliminar: " << endl;
				cin >> numJog;
				bd->eliminarJogador(numJog);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcM != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuConfiguracao()
{
	char opcConfiguracao;
	BaseDados *bd = new BaseDados();
	Configuracao *conf = new Configuracao();
	string codConfig;
	char resp;
	do
	{
		cout << endl << "********************MENU CONFIGURACAO****************" << endl;
		cout << "1) Informacao configuracao" << endl << "2) Inserir configuracao" << endl << "3) Eliminar configuracao" << endl << "4) Retornar menu principal" << endl << "*****************************************************" << endl;
		opcConfiguracao = lerOpcao();
		char opcC;
		switch (opcConfiguracao)
		{
		case '1':
			//lerConfiguracao da base de dados
			do
			{
				string codeConfig;
				cout << endl << "***************INFORMACAO DA CONFIGURACAO***************" << endl;
				cout << "1) Listar configuracoes " << endl << "2) Visualizar informacao de uma configuracao" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu configuracoes" << endl << "*********************************************" << endl;
				opcC = lerOpcao();
				switch (opcC)
				{
				case '1': //Listar Configuracoes
					bd->listarConfiguracao();
					break;

				case '2': //Visualizar uma configuracao
					cout << "Introduza o codigo da configuracao que pretende visualizar: " << endl;
					cin >> codeConfig;
					if ((bd->lerConfiguracao(codeConfig)) == true)
					{
						bd->mostrarConfiguracao(codeConfig);
					}
					else
					{
						while ((bd->lerConfiguracao(codeConfig)) == false)
						{
							cout << "Insira o numero da equipa que pretende visualizar: ";
							cin >> codeConfig;
						}
					}
					break;

				case '3': //Atualizar informacao
						  //primeiro e necessario saber qual a configuracao que pretende
					cout << "Introduza o codigo da configuracao que pretende atualizar informacao: " << endl;
					cin >> codeConfig;
					if ((bd->lerConfiguracao(codeConfig)) == true)
					{
						bd->atualizarConfiguracao(codeConfig);
					}
					else
					{
						while ((bd->lerConfiguracao(codeConfig)) == false)
						{
							cout << "Insira o numero da equipa que pretende atualizar: ";
							cin >> codeConfig;
						}
					}
					break;

				case '4': //retorna ao menu configuracoes
					menuConfiguracao();
					break;
				case '0':
				default:
					cout << "Opcao invalida! Insira novamente a opcao. ";
					break;
				}
			} while (opcC != 4);

		break;
		case '2':
			//inserir configuracao robo (base de dados)
			cout << "Introduza o codigo da configuracao: " << endl;
			cin >> codConfig;
			if ((bd->lerConfiguracao(codConfig)) == true)
			{
				while ((bd->lerConfiguracao(codConfig)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codConfig;
				}
			}
			else
			{ 
				conf->preencherConfiguracao(codConfig);
				bd->inserirConfiguracao(*conf);
			}
			break;
		case '3':
			//eliminar configuracao robo (base de dados)
			cout << "Introduza o codigo da configuracao que pretende eliminar: " << endl;
			cin >> codConfig;
			bd->eliminarConfiguracao(codConfig);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcConfiguracao != 4);
}

#include "Jogador.h"
#include <iostream>

Jogador::Jogador()
{

}

Jogador::Jogador(const Jogador &jog)
{
	nomeJogador = jog.nomeJogador;
	idadeJog = jog.idadeJog;
	numEquipa = jog.numEquipa;
	numJogador = jog.numJogador;

}

Jogador::Jogador(string nomePlayer, int idadePlayer,int numeroTeam, int numeroPlayer) :
	nomeJogador(nomePlayer), idadeJog(idadePlayer), numEquipa(numeroTeam), numJogador(numeroPlayer)
{

}

void Jogador::setNomeJogador(string nameJog)
{
	nomeJogador = nameJog;
}

void Jogador::setIdadeJogador(int age)
{
	idadeJog = age;

}

void Jogador::setNumeroEquipa(int numTeam)
{
	numEquipa = numTeam;

}

void Jogador::setNumeroJogador(int numPlayer)
{
	numJogador = numPlayer;
}

string Jogador::getNomeJogador()const
{
	return nomeJogador;
}

int Jogador::getIdadeJogador() const
{
	return idadeJog;
}

int Jogador::getNumeroEquipa() const
{
	return numEquipa;
}

int Jogador::getNumeroJogador() const
{
	return numJogador;
}


void Jogador::preencherJogador(int numJog)
{
	Jogador *jog = new Jogador();


	cout << "Introduza o nome do jogador: " << endl;
	cin >> nomeJogador;
	cout << "Introduza a idade do jogador: " << endl;
	cin >> idadeJog;
	cout << "Introduza o numero da equipa do jogador:  " << endl;
	cin >> numEquipa;

	numJogador = numJog;

}
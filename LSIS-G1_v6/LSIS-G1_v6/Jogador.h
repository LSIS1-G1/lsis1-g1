#pragma once

#ifndef jogador_
#define Jogador_

#include <iostream>
#include <string>

using namespace std;

class Jogador
{

private:
	string nomeJogador;
	int idadeJog;
	int numEquipa;
	int numJogador;

public:
	Jogador();
	Jogador(const Jogador &player);
	Jogador(string nameJog, int ageJog,int numberEquipa, int numberJog);

	void setNomeJogador(string nameJog);
	void setIdadeJogador(int ageJog);
	void setNumeroEquipa(int numberEquipa);
	void setNumeroJogador(int numberJog);

	string getNomeJogador()const;
	int getIdadeJogador()const;
	int getNumeroEquipa()const;
	int getNumeroJogador()const;

	void preencherJogador(int numJog);
};

#endif
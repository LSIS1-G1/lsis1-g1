#include "Prova.h"
#include <iostream>

Prova::Prova()
{

}

Prova::Prova(const Prova &pr)
{
	codigo = pr.codigo;
	numEquipa = pr.numEquipa;
	data = pr.data;
	hora = pr.hora;
	local = pr.local;
	tempo = pr.tempo;
	posicao = pr.posicao;

}
Prova::Prova(string code, int numTeam, string date, string hour, string localP, int time, string posicionamento) :
	codigo(code),numEquipa(numTeam), data(date), hora(hour), local(localP), tempo(time), posicao(posicionamento)
{

}
void Prova::setCodigo(string code)
{
	codigo = code;
}
void Prova::setNumeroEquipa(int team)
{
	numEquipa = team;
}
void Prova::setData(string date)
{
	data = date;
}
void Prova::setHora(string hour)
{
	hora = hour;
}
void Prova::setTempos(int time)
{
	tempo = time;
}
void Prova::setLocal(string localP)
{
	local = localP;
}
void Prova::setPosicao(string posicionamento)
{
	posicao = posicionamento;
}
string Prova::getCodigo() const
{
	return codigo;
}
int Prova::getNumeroEquipa() const
{
	return numEquipa;
}
string Prova::getData() const
{
	return data;
}
string Prova::getHora() const
{
	return hora;
}
string Prova::getLocal() const
{
	return local;
}
string Prova::getPosicao() const
{
	return posicao;
}
int Prova::getTempos() const
{
	return tempo;
}

void Prova::preencherProva(string iD)
{
	string  dataProva, horaProva, localProva, posicao;
	int tempos, numEquipa;
	Prova *pr = new Prova();


	cout << "Introduza o numero da equipa: " << endl;
	cin >> numEquipa;
	cout << "Introduza a data da prova: " << endl;
	cin >> dataProva;

	cout << "Introduza a hora da prova:  " << endl;
	cin >> horaProva;
	cout << "Introduza o local da prova: " << endl;
	cin >> localProva;

	cout << "Introduza a duracao da prova: " << endl;
	cin >> tempos;
	cout << "Introduza o posicionamento da equipa: " << endl;
	cin >> posicao;

	pr = new Prova (iD, numEquipa, dataProva, horaProva, localProva, tempos, posicao);

}
#ifndef BaseDados_
#define BaseDados_


#include <iostream>
#include <string>
#include <exception>

#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/sqlstring.h>


using namespace std;

#include "Equipa.h"
#include "Robo.h"
#include "Prova.h"
#include "Configuracao.h"
#include "Jogador.h"


using namespace sql;


class BaseDados {
private:

	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
	string nomeBD;

public:
	BaseDados();
	~BaseDados();
	//BaseDados(string nome);

	void ligar();
	void fechar();

	//METODOS RELATIVOS A EQUIPA
	bool lerEquipa(int numEqui);
	void listarEquipas();
	void inserirEquipa(Equipa &eq);
	void eliminarEquipa(int numEquipa);
	void mostrarEquipa(int numEquipa);
	void atualizarEquipas(int codeTeam);

	//METODOS RELATIVOS AO ROBO
	bool lerRobo(string codeRobo);
	void listarRobo();
	void mostrarRobo(string code);
	void inserirRobo(Robo &robot);
	void eliminarRobo(string codeRobo);
	void atualizarRobo(string codeR);

	//METODOS RELATIVOS A PROVAS
	bool lerProva(string codePro);
	void listarProvas();
	void mostrarProva(string codeP);
	void inserirProva(Prova &prov);
	void eliminarProva(string codeP);
	void atualizarProva(string codeProva);

	//METODOS RELATIVOS A CONFIGURACOES
	bool lerConfiguracao(string code);
	void listarConfiguracao();
	void mostrarConfiguracao(string code);
	void inserirConfiguracao(Configuracao &confi);
	void eliminarConfiguracao(string code);
	void atualizarConfiguracao(string codeR);

	//METODOS RELATIVOS A JOGADORES
	bool lerJogador(int numJog);
	void listarJogadores();
	void mostrarJogador(int numJog);
	void inserirJogador(Jogador &jog);
	void eliminarJogador(int numJog);
	void atualizarJogador(int numJog);

};
#endif


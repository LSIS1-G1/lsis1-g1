#include "BaseDeDados.h"
#include "utils.h"
BaseDados::BaseDados()
{
	nomeBD = "V2";
	ligar();
}

BaseDados::~BaseDados() {
	con->close();
	delete stmt;
	delete con;

}

void BaseDados::ligar() {
	cout << "A ligar..."<<endl;
	driver = get_driver_instance();
	try {
		con = driver->connect("tcp://127.0.0.1:3306", "root", "");
		con->setSchema(nomeBD);
		stmt = con->createStatement();
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}
void BaseDados::fechar()
{
	BaseDados::~BaseDados();
}


/////////////////////////VERIFICA SE A EQUIPA EXISTE///////////////////////////////////////
bool BaseDados::lerEquipa(int numEqui) {
	//string numEquipa_str = numberToString(numEqui);
	try
	{
		res = stmt->executeQuery("SELECT * from equipas where NumEquipa = " + numEqui);
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

	if (res->rowsCount() == 1) {
		res->next();
	/*	equipa.setNomeEq(res->getString("Nome"));
		equipa.setNumEquipa(res->getInt("NumEquipa"));
		equipa.setNumElementos(res->getInt("NumElementos"));
		equipa.setLema(res->getString("Lema"));
		equipa.setLocalidade(res->getString("Localidade"));*/
		delete res;
	}
	else
		return false;
	return true;
}



/////////////////////////////////////METODOS PARA A TABELA EQUIPA////////////////////////////////////////////

/////////////////////////////////MOSTRAR APENAS UMA EQUIPA///////////////////////////////
void BaseDados::mostrarEquipa(int numEquipa)
{
	try
	{
		res = stmt->executeQuery("SELECT * from equipas where NumEquipa = " + numEquipa);
		cout << "Operacao realizada com sucesso!" << endl;
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}
}

/////////////////////////////////ATUALIZAR UMA EQUIPA/////////////////////////////////////
void BaseDados::atualizarEquipas(int numEquipa)
{
	
	int opc = 0, newNumEl;
	string newName, newLema, newLocal;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome da equipa" << endl << "2) Numero de elementos" << endl << "3) Lema da equipa" << endl << "4) Localidade da equipa" << endl <<"5 )Voltar a INFORMACAO DA EQUIPA" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar nome da equipa
			cout << "Introduza o novo nome da equipa: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("update equipas set Nome = "+newName+"where NumEquipa = "+ numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar numero de elementos
			cout << "Introduza o novo numero de elementos na equipa: " << endl;
			cin >> newNumEl;
			try {
				stmt->executeUpdate("UPDATE equipas set NumElementos = " + numberToString(newNumEl) + "where NumEquipa = " + numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar lema da equipa
			cout << "Introduza o novo lema da equipa: " << endl;
			cin >> newLema;

			try {
				stmt->executeUpdate("update equipas set Lema = " + newLema + "where NumEquipa = "+numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //alterar localidade da equipa
			cout << "Introduza a nova localidade da equipa: " << endl;
			cin >> newLocal;
			try {
				stmt->executeUpdate("UPDATE equipas set Localidade = " + newLocal+ "where NumEquipa = " + numberToString(numEquipa));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: //volta ao menu informacao da equipa

			break;
	
		default: 
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;
		}
	} while (opc != 5);
}

////////////////////////////////LISTAR EQUIPAS////////////////////////////////////////////////
void BaseDados::listarEquipas()
{
	try
	{
		res = stmt->executeQuery("SELECT * from  equipas");
		cout << "Operacao realizada com sucesso!" << endl;
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////INSERIR UMA EQUIPA NA TABELA equipas///////////////////////////////////////////
void BaseDados::inserirEquipa(Equipa &eq) 
{
	try
	{
			stmt->execute("insert into equipas(Nome, NumElementos, Lema, Localidade, NumEquipa)values('" +eq.getNomeEq()+"," + numberToString(eq.getNumElementos()) + ","+  eq.getLema() + "," + eq.getLocalidade() + ","+numberToString(eq.getNumEquipa())+")");
			
			cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

///////APAGAR UMA EQUIPA NA TABELA equipas////////////////////////////////////
void BaseDados::eliminarEquipa(int numEquipa)
{
	try
	{
		stmt->executeUpdate("DELETE from equipas where NumEquipa = " + numEquipa);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////////////////////ROBO//////////////////////////////////////////////////////////////////////////

//////////////////////////LER ROBO/////////////////////////////////////////////////////////////
bool BaseDados::lerRobo(string codeRobo)
{
	try
	{
		res = stmt->executeQuery("SELECT * from rob� where Codigo = " + codeRobo);
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

	if (res->rowsCount() == 1) {
		res->next();
	/*	robot.setNome(res->getString("Nome"));
		robot.setCodigo(res->getString("Codigo"));
		robot.setEquipa(res->getString("Equipa"));
		robot.setCor(res->getString("Cor"));
		robot.setFuncaoP(res->getString("FuncaoP"));
		//ou res->getString(3);
		*/
		delete res;
	}
	else
		return false;
	return true;
}

////////////////////////////////LISTAR ROBOS///////////////////////////////////////////
void BaseDados::listarRobo()
{
	try
	{
		res = stmt->executeQuery("SELECT * from rob� ");
		cout << "Operacao realizada com sucesso!" << endl;
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}

////////////////////////////////MOSTRAR UM ROBO //////////////////////////////////////
void BaseDados::mostrarRobo(string codigoR)
{
	try
	{
		res = stmt->executeQuery("SELECT * from rob� where Codigo = " + codigoR);
	}
	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

}

/////////////////////////////////ATUALIZAR INFORMACAO DO ROBO////////////////////////////
void BaseDados::atualizarRobo(string codeR)
{

	int opc = 0, newTeam=0;
	string newName, newColour, newFunction;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome do robo" << endl << "2) Equipa do robo" << endl << "3) Cor do robo" << endl << "4) Funcao principal" <<  endl << "5) Voltar a INFORMACAO DO ROBO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar nome do robo 
			cout << "Introduza o novo nome do robo: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("UPDATE rob� set Nome = " + newName + "where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a equipa do robo
			cout << "Introduza o numero da nova equipa: " << endl;
			cin >> newTeam;
			try {
				stmt->executeUpdate("UPDATE rob� set  NumEquipa = " + numberToString(newTeam) + "where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar cor do robo
			cout << "Introduza a nova cor do robo: " << endl;
			cin >> newColour;

			try {
				stmt->executeUpdate("UPDATE rob� set Cor = " + newColour + "where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //alterar funcao principal do robo
			cout << "Introduza a nova funcao principal do robo: " << endl;
			cin >> newFunction;
			try {
				stmt->executeUpdate("UPDATE rob� set FuncaoP = " + newFunction + "where Codigo = " + codeR);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: //voltar ao menu INFORMACAO DO ROBO
			
			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;


		}
	} while (opc != 5);
}

////////////////////////////////INSERIR NA TABELA rob�////////////////////////////////
void BaseDados:: inserirRobo (Robo &ro)
{
	try
	{
		stmt->execute("insert into rob�(Nome, Codigo, NumEquipa, Cor, FuncaoP)values(" + ro.getNome() + "," + ro.getCodigo() + "," + numberToString(ro.getNumeroEquipa()) + "," + ro.getCor() + "," + ro.getFuncaoP() + ")");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////ELIMINAR NA TABELA rob�/////////////////////////////////
void BaseDados::eliminarRobo(string codeRobot)
{
	try
	{
		stmt->executeUpdate("DELETE from rob� where Codigo = " + codeRobot);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

///////////////////////////////////////////////////////////////////////////PROVAS//////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////LER PROVA//////////////////////////////////////////////////////
bool BaseDados::lerProva(string codeProva)
{
	try
	{
		res = stmt->executeQuery("SELECT * from provas where Codigo = " + codeProva);
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

	if (res->rowsCount() == 1) {
		res->next();
		/*pr.setCodigo(res->getString("Nome"));
		pr.setEquipa(res->getString("Equipa"));
		pr.setData(res->getString("DataProva"));
		pr.setHora(res->getString("HoraProva"));
		pr.setLocal(res->getString("LocalProva"));
		pr.setTempos(res->getInt("TempoProva"));
		pr.setPosicao(res->getString("Posicionamento"));
		//ou res->getString(3);*/
		delete res;
	}
	else
		return false;
	return true;
}

////////////////////////////////////ATUALIZAR PROVAS///////////////////////////////////////////////
void BaseDados::atualizarProva(string codePro)
{

	int opc = 0, newTime, newTeam;
	string  newDate, newHour, newLocal, newPos;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Equipa" << endl << "2) Data da prova" << endl << "3) Hora da prova" << endl << "4) Local da prova" << endl <<"5) Tempo da prova"<<endl << "6) Posicionamento"<< endl << "7) Voltar a INFORMACAO DA PROVA" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar equipa
			cout << "Introduza o numero da nova equipa: " << endl;
			cin >> newTeam;
			try {
				stmt->executeUpdate("UPDATE provas set Equipa = " + numberToString(newTeam )+ "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a data da prova
			cout << "Introduza a nova data: " << endl;
			cin >> newDate;
			try 
			{
				stmt->executeUpdate("UPDATE provas set  DataProva = " + newDate + "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar a hora da prova
			cout << "Introduza a nova hora da prova: " << endl;
			cin >> newHour;

			try
			{
				stmt->executeUpdate("UPDATE provas set HoraProva = " + newHour + "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //alterar o local da prova
		
			cout << "Introduza o novo local da prova: " << endl;
			cin >> newLocal;
			try 
			{
				stmt->executeUpdate("UPDATE provas set LocalProva = " + newLocal + "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 5: //alterar o tempo de prova
			cout << "Introduza o novo tempo de duracao da prova" << endl;
			cin >> newTime;

			try
			{
				stmt->executeUpdate("UPDATE provas set TempoProva = "+numberToString(newTime)+ "where Codigo = " + codePro);
				cout << "Operacao realizada com sucesso!";
			}
			catch (SQLException &e)
			{
				cout << "Operacao realizada com sucesso!";
			}

			break;

		case 6: //alterar o posicionamento da equipa na prova
			cout << "Introduza a nova posicao da equipa nas provas";
			cin >> newPos;

			try
			{
				stmt->executeUpdate("UPDATE provas set Posicionamento = "+newPos+ "where Codigo = " + codePro);
			}
			catch (SQLException &e)
			{
				cout << "Operacao realizada com sucesso!";
			}

			break;

		
		case 7: //volta ao menu informacao da equipa
			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;


		}
	} while (opc !=7 );
}
///////////////////////////////////LISTAR PROVAS///////////////////////////////////////////////////
void BaseDados::listarProvas()
{
	try
	{
		res = stmt->executeQuery("SELECT * FROM provas");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////MOSTRAR PROVA//////////////////////////////////////////////////
void BaseDados::mostrarProva(string code)
{
	try
	{
		res = stmt->executeQuery("SELECT * from provas where Codigo = " + code);
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}
///////////////////////////////////INSERIR NA TABELA provas////////////////////////////////////////
void BaseDados::inserirProva(Prova &pr)
{
	try
	{
		stmt->execute("insert into provas(Codigo, NumEquipa, DataProva, HoraProva, LocalProva, TempoProva, Posicionamento) values ("+pr.getCodigo()+","+numberToString(pr.getNumeroEquipa())+","+ pr.getData()+","+pr.getHora()+","+pr.getLocal()+","+numberToString(pr.getTempos())+","+pr.getPosicao()+")");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;

	}
}

////////////////////////////////////ELIMINAR NA TABELA provas/////////////////////////////////////////
void BaseDados::eliminarProva(string codigo )
{
	try
	{
		stmt->executeUpdate("DELETE * from provas where Codigo = " + codigo);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////////////////////////////CONFIGURACAO//////////////////////////////////////////////////////

///////////////////////////////////////LER CONFIGURACOES/////////////////////////////////////////////
bool BaseDados::lerConfiguracao(string code)
{
	try
	{
		res = stmt->executeQuery("SELECT * from configuracao where Codigo = " + code);
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

	if (res->rowsCount() == 1) {
		res->next();
		/*config.setSensor(res->getString("Sensores"));
		config.setVelocidade(res->getInt("Velocidade"));
		config.setFuncoes(res->getString("Funcoes"));
		
		*/
		delete res;
	}
	else
		return false;
	return true;
}

///////////////////////////////////////ATUALIZAR CONFIGURACOES///////////////////////////////////////
void BaseDados::atualizarConfiguracao(string codeConf)
{

	int opc = 0;
	string newSensor, newVeloc, newFunctions;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Tipo de sensores" << endl << "2) Velocidade maxima" << endl << "3) Funcoes extras"  << endl << "4) Voltar a INFORMACAO DA CONFIGURACAO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar os tipos de sensores 
			cout << "Introduza os novos tipos de sensores: " << endl;
			cin >> newSensor;
			try {
				stmt->executeUpdate("UPDATE configuracao set Sensores = " + newSensor + "where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a velocidade maxima 
			cout << "Introduza a nova velocidade maxima: " << endl;
			cin >> newVeloc;
			try {
				stmt->executeUpdate("UPDATE configuracao set  Velocidade = " + newVeloc + "where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar as funcoes extras 
			cout << "Introduza as novas funcoes do robo: " << endl;
			cin >> newFunctions;

			try {
				stmt->executeUpdate("UPDATE configuracao set Funcoes = " + newFunctions + "where Codigo = " + codeConf);
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		
	
		case 4: //volta ao menu informacao das configuracoes
			break;

		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;


		}
	} while (opc != 4);
}


///////////////////////////////////////LISTAR CONFIGURACOES//////////////////////////////////////////
void BaseDados::listarConfiguracao()
{
	try
	{
		res = stmt->executeQuery("SELECT * FROM configuracao");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

///////////////////////////////////////MOSTRAR CONFIGURACOES////////////////////////////////////////
void BaseDados::mostrarConfiguracao(string code)
{
	try
	{
		res = stmt->executeQuery("SELECT * from configuracao where Codigo = " + code);
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}


//////////////////////////////////////INSERIR NA TABELA configuracao////////////////////////////////
void BaseDados::inserirConfiguracao(Configuracao &confi)
{
	try
	{
		stmt->execute("insert into configuracao(Codigo, Sensores, Velocidade, Funcoes) values (" + confi.getCodigo() + "," + confi.getSensor() + "," + numberToString(confi.getVelocidade()) + "," + confi.getFuncoes()+ ")");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////////////////////////////////////ELIMINAR NA TABELA configuracoes//////////////////////////////
void BaseDados::eliminarConfiguracao(string codigo)
{
	try
	{
		stmt->executeUpdate("DELETE * from configuracao where Codigo = " + codigo);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////////////////////////////////////////JOGADORES//////////////////////////////////////////////////

////////////////////////////////////////INSERIR NA TABELA jogadores /////////////////////////////////
void BaseDados::inserirJogador(Jogador &jog)
{
	try
	{
		stmt->execute("insert into jogadores (NomeJogador, Idade, NumEquipa, NumJogador) values (" + jog.getNomeJogador() + "," + numberToString(jog.getIdadeJogador()) + "," + numberToString(jog.getNumeroEquipa()) + "," + numberToString(jog.getNumeroJogador()) + ")");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

////////////////////////////////////////ELIMINAR NA TABELA jogadores////////////////////////////////
void BaseDados::eliminarJogador(int numJog)
{
	try
	{
		stmt->executeUpdate("DELETE * from jogadores where NumJogador = " + numJog);
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}
//////////////////////////////////////////MOSTRAR jogador/////////////////////////////////////////////
void BaseDados::mostrarJogador(int jog)
{
	try
	{
		stmt->executeQuery("SELECT * from jogadores where NumJog = " + jog);
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}

}

//////////////////////////////////////////LISTAR JOGADORES////////////////////////////////////////////
void BaseDados::listarJogadores()
{
	try
	{
		res = stmt->executeQuery("SELECT * FROM jogadores");
		cout << "Operacao realizada com sucesso!";
	}
	catch (SQLException &e)
	{
		cout << "Erro: " << e.what() << endl;
	}
}

//////////////////////////////////////////LER JOGADOR//////////////////////////////////////////////////
bool BaseDados::lerJogador(int numJog)
{
	try
	{
		res = stmt->executeQuery("SELECT * from jogadores where NumJogador = " + numJog);
	}

	catch (SQLException &e)
	{
		cout << "#SQL Exception: " << e.what();
		cout << " MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " " << endl;
	}

	if (res->rowsCount() == 1) {
		res->next();
		/*pr.setCodigo(res->getString("Nome"));
		pr.setEquipa(res->getString("Equipa"));
		pr.setData(res->getString("DataProva"));
		pr.setHora(res->getString("HoraProva"));
		pr.setLocal(res->getString("LocalProva"));
		pr.setTempos(res->getInt("TempoProva"));
		pr.setPosicao(res->getString("Posicionamento"));
		//ou res->getString(3);*/
		delete res;
	}
	else
		return false;
	return true;
}

//////////////////////////////////////////////ATUALIZAR JOGADOR////////////////////////////////////////////////////
void BaseDados::atualizarJogador(int numJog)
{

	int opc = 0, newAge = 0, numTeam =0;
	string newName;
	do
	{
		cout << "O que pretende alterar?" << endl;
		cout << "1) Nome do jogador" << endl << "2) Idade do jogador" << endl << "3) Numero de equipa do jogador" << endl << "4) Voltar a INFORMACAO DO ROBO" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1: //alterar nome do jogador
			cout << "Introduza o novo nome do jogador: " << endl;
			cin >> newName;
			try {
				stmt->executeUpdate("UPDATE jogadores set NomeJogador = " + newName + "where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;

		case 2: //alterar a idade do jogador
			cout << "Introduza a nova idade do jogador: " << endl;
			cin >> newAge;
			try {
				stmt->executeUpdate("UPDATE jogadores set  Idade = " + numberToString(newAge) + "where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 3: //alterar o numero da equipa do jogador
			cout << "Introduza o novo numero da equipa do jogador: " << endl;
			cin >> numTeam;

			try {
				stmt->executeUpdate("UPDATE  set NumEquipa = " + numberToString(numTeam) + "where NumJogador = " + numberToString(numJog));
				cout << "Operacao realizada com sucesso!";

			}
			catch (SQLException &e)
			{
				cout << "Erro: " << e.what() << endl;
			}
			break;
		case 4: //voltar ao menu INFORMACAO DO JOGADOR

			break;
		default:
			cout << "Valor inv�lido! Volte a inserir a opcao que pretende. " << endl;
			break;


		}
	} while (opc != 4);
}



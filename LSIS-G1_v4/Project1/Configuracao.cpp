#include "Configuracao.h"
#include <iostream>

Configuracao::Configuracao()
{

}
Configuracao::Configuracao(const Configuracao &con)
{
	sensor = con.sensor;
	velocidade = con.velocidade;
	funcoes = con.velocidade;
	codigoR = con.codigoR;
}
Configuracao::Configuracao(string codigo, string sensores, int veloc, string functions) :
	codigoR(codigo),sensor(sensores), velocidade(veloc), funcoes(functions)
{

}
void Configuracao::setSensor(string sensores)
{
	sensor = sensores;
}
void Configuracao::setVelocidade(int veloc)
{
	velocidade = veloc;
}
void Configuracao::setFuncoes(string functions)
{
	funcoes = functions;
}
void Configuracao::setCodigo(string codigo)
{
	codigoR = codigo;
}
string Configuracao::getSensor() const
{
	return sensor;
}
int Configuracao::getVelocidade() const
{
	return velocidade;
}
string Configuracao::getFuncoes() const
{
	return funcoes;
}
string Configuracao::getCodigo() const
{
	return codigoR;
}

void Configuracao::preencherConfiguracao(string codigo)
{
	string sensores, funcoes;
	int velocidade;
	Configuracao *conf = new Configuracao();


	cout << "Introduza os sensores que o robo possui: " << endl;
	cin >> sensores;
	cout << "Introduza a velocidade maxima que o robo atinge: " << endl;
	cin >> velocidade;

	cout << "Introduza as funcoes extras do robo: " << endl;
	cin >> funcoes;
	

	conf = new Configuracao(codigo, sensores, velocidade, funcoes);

}

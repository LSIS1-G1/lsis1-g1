#ifndef Prova_
#define Prova_

#include <string>
using namespace std;

class Prova
{
private:
	string data;
	string hora;
	string local;
	int tempo;
	string posicao;
	string codigo;
	int numEquipa;
	
public:
	Prova();
	Prova(const Prova &pr);
	Prova(string code, int numTeam, string date, string hour, string localP, int time, string posicionamento);
	void setCodigo(string code);
	void setNumeroEquipa(int numTeam);
	void setData(string date);
	void setHora(string hour);
	void setLocal(string localP);
	void setTempos(int time);
	void setPosicao(string posicionamento);
	string getCodigo()const;
	int getNumeroEquipa()const;
	string getData() const;
	string getHora() const;
	string getLocal() const;
	int getTempos() const;
	string getPosicao() const;
	void preencherProva(string code);

};

#endif

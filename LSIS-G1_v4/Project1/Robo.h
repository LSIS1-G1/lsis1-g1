#ifndef Robo_
#define Robo_

#include <string>
using namespace std;

class Robo
{
	private:
		string nome;
		string codigo;
		int numEquipa;
		string cor;
		string funcaoP;

	public:
		Robo();
		Robo(const Robo &r);
		Robo(string name, string code, int numTeam, string color, string functionP);
		void setNome(string name);
		void setCodigo(string code);
		void setNumeroEquipa(int codeTeam);
		void setCor(string color);
		void setFuncaoP(string functionP);
		void preencherRobo(string codiRobo);
		string getNome() const;
		string getCodigo() const;
		int getNumeroEquipa() const;
		string getCor() const;
		string getFuncaoP() const;
};
#endif

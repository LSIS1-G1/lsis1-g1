#ifndef Configuracao_
#define Configuracao_

#include <string>
using namespace std;

class Configuracao
{
private:
	string sensor;
	int velocidade;
	string funcoes;
	string codigoR;

public:
	Configuracao();
	Configuracao(const Configuracao &con);
	Configuracao(string codigo, string sensores, int veloc, string functions);
	void setSensor(string sensores);
	void setVelocidade(int veloc);
	void setFuncoes(string functions);
	void setCodigo(string codigo);
	string getSensor() const;
	int getVelocidade() const;
	string getFuncoes() const;
	string getCodigo() const;
	void preencherConfiguracao(string codigo);

};
#endif


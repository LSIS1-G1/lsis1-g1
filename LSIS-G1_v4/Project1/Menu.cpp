
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>

using namespace std;

#include "Menu.h"
#include "Equipa.h"
#include "BaseDeDados.h"
#include "Robo.h"
#include "utils.h"
#include "Configuracao.h"
#include "Jogador.h"

void Menu::menuPrincipal()
{
	int opc;
	bool programa = true;
	do
	{
		cout << "******************** MENU PRINCIPAL ****************" << endl;
		cout << "1) Equipa" << endl << "2) Robo" << endl << "3) Prova" << endl << "4) Jogadores" << endl << "5) Configuracao" << endl << "6) Creditos" << endl << "7) Encerrar o programa" <<endl<<"****************************************************" << endl;
		cin >> opc;
		switch (opc)
		{
		case 1:
			//inserir menu equipa
			menuEquipa();
			break;
		case 2:
			//inserir menu robo
			menuRobo();
			break;
		case 3:
			//inserir menu prova
			menuProva();
			break;
		case 4:
			//inserir menu jogadores
			//menuJogadores();
			break;
		case 5:
			//inserir menu configuracao
			menuConfiguracao();
			break;
		case 6:
			cout << "No ambito da disciplina Laboratorios de Sistemas 1, da Licenciatura em Engenharia de Sistemas, foi proposto a realiza��o de um robo bombeiro e de um sistema de informacao para gerir toda a informacao para uma competicao deste tipo." << endl;
			cout << "Docentes: " << endl << "Andre Dias" << endl << "Alberto Sampaio" << endl;
			cout << "2015/2016" << endl;
			cout << "Trabalho realizado por: " << endl << "Ana Nogueira - 1140476" << endl << "Daniel Barbosa - 1141251" << endl << "Diogo Monteiro - 1140544" << endl << "Pedro Ribeiro - 1141188" << endl;
		
			break;
		case 7: //Encerramento do programa
			cout << "Programa terminado."<<endl;
			programa = false;
			break;

		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opc !=7);

}

void Menu::menuEquipa()
{
	int opcEquipa, numEquipa = 1, nEquipa = 0, opcEq = 0;
	string NomeEquipa, lema, Localidade;
	char resp;
	Equipa* equi = new Equipa();
	BaseDados* bd = new BaseDados();
	do {

		cout << "********************MENU EQUIPA****************" << endl;
		cout << "1) Informacao equipa" << endl << "2) Inserir equipa" << endl << "3) Eliminar equipa" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
		cin >> opcEquipa;
		switch (opcEquipa)
		{
		case 1:

			//lerEquipa da base de dados
			do 
			{
				int codeTeam;
				cout << "***************INFORMACAO DA EQUIPA***************"<<endl;
				cout << "1) Listar equipas " << endl << "2) Visualizar uma equipa" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu equipa" << endl << "***********************************************" <<endl;
				cin >> opcEq;
				switch (opcEq)
				{
				case 1://listar equipas
					bd->listarEquipas();
					break;
				case 2: //visualizar toda a informacao da equipa
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeTeam;
					bd->mostrarEquipa(codeTeam);
					break;
				case 3: //atualizar informacao
					//primeiro tem de saber qual a equipa que pretende
					cout << "Introduza o numero da equipa: " << endl;
					cin >> codeTeam;

					//verifica se a equipa existe e so depois e que passa para a atualizacao
									
					if (bd->lerEquipa(codeTeam) == true)
						bd->atualizarEquipas(codeTeam);
					else
						cout << "Equipa n�o registada!" << endl;

					//ESPEREMOS QUE ISTO ASSIM FUNCIONE ANA NOGUEIRA!!!!!!!!!!!!!!!!!!!!!!!


					break;
				case 4://retorna ao menu principal
					break;
				default:
					cout << "Opcao invalida! Insira novamente uma opcao viavel." << endl;
					break;
				}

			} while (opcEq != 4);

			break;
		case 2:
			//inserir equipa (base de dados)
			cout << "Introduza o numero da equipa: " << endl;
			cin >> nEquipa;

			equi->preencherEquipa(nEquipa);
			bd->inserirEquipa(*equi);

			break;

		case 3:
			//eliminar equipa (base de dados)
			do
			{
				cout << "Indique o numero de equipa que pretende eliminar: ";
				cin >> nEquipa;
				cout << "Tem a certeza que pretende eliminar a equipa numero : " + numberToString(nEquipa) + " ?" << endl;
				bd->mostrarEquipa(nEquipa);
				cout << "S -Sim" << endl << "N -Nao" << endl;
				cin >> resp;
				if (toupper(resp) != 'S' || toupper(resp) != 'N')
					cout << "Opcao invalida!";
			} while (toupper(resp) != 'S');
			
				bd->eliminarEquipa(nEquipa);
			
			break;

		case 4:
			menuPrincipal();
			break;
		default:
			cout << "Opcao invalida! Insira nova opcao!" << endl;
			break;

		}
	} while (opcEquipa != 4);
}

void Menu::menuRobo()
{
	int opcRobo;
	string codeRobo;
	char resp;
	Robo *robot = new Robo();
	BaseDados *bd = new BaseDados();
	int opcR = 0;
	do {
		cout << "********************MENU ROBO****************" << endl;
		cout << "1) Informacao robo" << endl << "2) Inserir robo" << endl << "3) Eliminar robo" << endl << "4) Retornar menu principal" << endl << "*********************************************" << endl;
		cin >> opcRobo;
		switch (opcRobo)
		{
		case 1: //Informacao Robo
			do
			{
				string codeRobot;
				cout << "***************INFORMACAO DO ROBO***************" << endl;
				cout << "1) Listar robos " << endl << "2) Visualizar um robo" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu robo" << endl << "*********************************************"<<endl;
				cin >> opcR;
				switch (opcR)
				{
				case 1: //Listar Robos
					bd->listarRobo();
					break;

				case 2: //Visualizar um robo
					cout << "Introduza o codigo do robo que pretende visualizar: " << endl;
					cin >> codeRobot;

					bd->mostrarRobo(codeRobot);
					break;

				case 3: //Atualizar informacao
					//primeiro e necessario saber qual o robo
					cout << "Introduza o codigo do robo que pretende atualizar informacao: " << endl;
					cin >> codeRobot;

					//verifica se o robo existe e so depois e que passa para a atualizacao

					if (bd->lerRobo(codeRobot) == true)
						bd->atualizarRobo(codeRobot);
					else
						cout << "Equipa n�o registada!" << endl;


					//TER ATENCAO A ISTO ANA NOGUEIRA!!!


					break;
				case 4: //Retornar ao menu robo
					break;
				default:
					cout << "Opcao invalida! Volte a inserir a opcao." << endl;
					break;
				}

			} while (opcR != 4);
			
			break;
		case 2:
			//inserir robo (base de dados)
			cout << "Introduza o codigo do robo: " << endl;
			cin >> codeRobo;

			robot->preencherRobo(codeRobo);
			bd->inserirRobo(*robot);
			
			break;
		case 3:
			//eliminar robo (base de dados)
			do
			{
			cout << "Introduza o codigo do robo que pretende eliminar: " << endl;
			cin >> codeRobo;
			cout << "Tem a certeza que pretende eliminar o robo com o codigo: " + codeRobo + " ?" << endl;
			bd->mostrarRobo(codeRobo);
			cout << "S -Sim" << endl << "N -Nao" << endl;
			cin >> resp;
			if (toupper(resp) != 'S' || toupper(resp) != 'N')
				cout << "Opcao invalida!";
		} while (toupper(resp) != 'S');

			bd->eliminarRobo(codeRobo);
			break;
		case 4:
			menuPrincipal();
			break;
		default:
			cout << "Opcao invalida! Insira nova opcao!" << endl;
			break;

		}
	} while (opcRobo != 4);
}

void Menu::menuProva()
{
	BaseDados *bd = new BaseDados();
	Prova *prova = new Prova();
	string codProva;
	char resp;
	int opcProv=0;
	int opcP = 0;
	do
	{
	cout << "********************MENU PROVA****************" << endl;
	cout << "1) Informacao prova" << endl << "2) Inserir prova" << endl << "3) Eliminar prova" << endl << "4) Retornar menu principal" << endl << "**********************************************" << endl;
	cin >> opcProv;
	switch (opcProv)
	{
	case 1:
		//lerProva da base de dados
		do
		{
			string codeProva;
			cout << "***************INFORMACAO DA PROVA***************" << endl;
			cout << "1) Listar provas " << endl << "2) Visualizar informacao de uma prova" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu provas" << endl << "*********************************************" << endl;
			cin >> opcP;
			switch (opcP)
			{
			case 1: //Listar Robos
				bd->listarProvas();
				break;

			case 2: //Visualizar um robo
				cout << "Introduza o codigo da prova que pretende visualizar: " << endl;
				cin >> codeProva;

				bd->mostrarProva(codeProva);
				break;

			case 3: //Atualizar informacao
					  //primeiro e necessario saber qual a prova que se pretende atualizar
				cout << "Introduza o codigo da prova que pretende atualizar informacao: " << endl;
				cin >> codeProva;

				//ler para saber se existe
				//perguntar o que quer editar
				//fazer o update da prova

				if (bd->lerProva(codeProva) == true)
					bd->atualizarProva(codeProva);
				else
					cout << "Prova n�o registada!" << endl;

				//TER ATENCAO A ISTO ANA NOGUEIRA!!!
				break;

			case 4: //torna ao menu prova
				break;
			default:
				cout << "Opcao invalida! Introduza uma opcao valida!";
				break;
			}
		} while (opcP != 4);
	
		break;
	case 2:
		//inserir prova (base de dados)
		cout << "Introduza o codigo da prova: " << endl;
		cin >> codProva;

		prova->preencherProva(codProva);
		bd->inserirProva(*prova);
		break;
	case 3:
		//eliminar prova (base de dados)
		do
		{
			cout << "Introduza o codigo da prova que pretende eliminar: " << endl;
			cin >> codProva;
			cout << "Tem a certeza que pretende eliminar a prova com o codigo: " + codProva + " ?" << endl;
			bd->mostrarProva(codProva);
			cout << "S -Sim" << endl << "N -Nao" << endl;
			cin >> resp;
			if (toupper(resp) != 'S' || toupper(resp) != 'N')
				cout << "Opcao invalida!";
		} while (toupper(resp) != 'S');

		bd->eliminarProva(codProva);
		break;
	case 4:
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

		}
	} while (opcProv != 4);
}

void Menu::menuJogadores()
{
	BaseDados *bd = new BaseDados();
	Jogador *jog = new Jogador();
	int opcJo, numJog, opcM;
	char resp;
	do
	{
		cout << "********************MENU JOGADOR****************" << endl;
		cout << "1) Informacao do jogador" << endl << "2) Inserir jogador" << endl << "3) Eliminar jogador" << endl << "4) Atualizar jogador" << endl << "5) Retornar menu principal" << endl << "***********************************************" << endl;
		cin >> opcM;
		switch (opcM)
		{
		case 1:
			//lerJogador da base de dados

			do
			{
				int numJ;
				cout << "***************INFORMACAO DA PROVA***************" << endl;
				cout << "1) Listar provas " << endl << "2) Visualizar informacao de uma prova" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu provas" << endl << "*********************************************" << endl;
				cin >> opcJo;
				switch (opcJo)
				{
				case 1: //Listar jogadores
					bd->listarJogadores();
					break;

				case 2: //Visualizar um jogador
					cout << "Introduza o numreo do jogador que pretende visualizar: " << endl;
					cin >> numJ;

					bd->mostrarJogador(numJ);
					break;

				case 3: //Atualizar informacao
						//primeiro e necessario saber qual a prova que se pretende atualizar
					cout << "Introduza o numero do jogador que pretende atualizar informacao: " << endl;
					cin >> numJ;

					//ler para saber se existe
					//perguntar o que quer editar
					//fazer o update da prova

					if (bd->lerJogador(numJ) == true)
						bd->atualizarJogador(numJ);
					else
						cout << "Jogador n�o registado!" << endl;

					//TER ATENCAO A ISTO ANA NOGUEIRA!!!
					break;

				case 4: //torna ao menu jogadores
					break;
				default:
					cout << "Opcao invalida! Introduza uma opcao valida!";
					break;
				}
			} while (opcJo != 4);
			break;
		case 2:
			//inserir jogador (base de dados)
			cout << "Introduza o numero do jogador: " << endl;
			cin >> numJog;

			jog->preencherJogador(numJog);
			bd->inserirJogador(*jog);
			break;
		case 3:
			//eliminar jogador robo (base de dados)
			do
			{
				cout << "Introduza o numero do jogador que pretende eliminar: " << endl;
				cin >> numJog;
				cout << "Tem a certeza que pretende eliminar o jogador com o numero: " + numberToString(numJog) + " ?" << endl;
				bd->mostrarJogador(numJog);
				cout << "S -Sim" << endl << "N -Nao" << endl;
				cin >> resp;
				if (toupper(resp) != 'S' || toupper(resp) != 'N')
					cout << "Opcao invalida!";
			} while (toupper(resp) != 'S');

			bd->eliminarJogador(numJog);

			break;
		case 4:
			menuPrincipal();
			break;
		default:
			cout << "Opcao invalida! Insira nova opcao!" << endl;
			break;

		}
	} while (opcM != 4);
}

void Menu::menuConfiguracao()
{
	int opcC = 0;
	BaseDados *bd = new BaseDados();
	Configuracao *conf = new Configuracao();
	int opcConfiguracao = 0;
	string codConfig;
	char resp;
	do
	{
		cout << "********************MENU CONFIGURACAO****************" << endl;
		cout << "1) Informacao configuracao" << endl << "2) Inserir configuracao" << endl << "3) Eliminar configuracao" << endl << "4) Retornar menu principal" << endl << "*****************************************************" << endl;
		cin >> opcConfiguracao;
		switch (opcConfiguracao)
		{
		case 1:
			//lerConfiguracao da base de dados
			do
			{
				string codeConfig;
				cout << "***************INFORMACAO DA CONFIGURACAO***************" << endl;
				cout << "1) Listar configuracoes " << endl << "2) Visualizar informacao de uma configuracao" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu configuracoes" << endl << "*********************************************" << endl;
				cin >> opcC;
				switch (opcC)
				{
				case 1: //Listar Configuracoes
					bd->listarConfiguracao();
					break;

				case 2: //Visualizar uma configuracao
					cout << "Introduza o codigo da configuracao que pretende visualizar: " << endl;
					cin >> codeConfig;

					bd->mostrarConfiguracao(codeConfig);
					break;

				case 3: //Atualizar informacao
						  //primeiro e necessario saber qual a configuracao que pretende
					cout << "Introduza o codigo da configuracao que pretende atualizar informacao: " << endl;
					cin >> codeConfig;

					//ler para saber se existe : bd->lerConfiguracao(codeConfig, *conf);??
					//perguntar o que quer editar
					//fazer o update da configuracao

					if (bd->lerConfiguracao(codeConfig) == true)
						bd->atualizarConfiguracao(codeConfig);
					else
						cout << "Configuracao n�o registada!" << endl;

					//TER ATENCAO A ISTO ANA NOGUEIRA!!!
					break;
				case 4: //retorna ao menu configuracoes
					break;
				default:
					cout << "Opcao invalida! Introduza uma opcao valida!";
					break;
				}
			} while (opcC != 4);

			break;
		case 2:
			//inserir configuracao robo (base de dados)
			cout << "Introduza o codigo da configuracao: " << endl;
			cin >> codConfig;

			conf->preencherConfiguracao(codConfig);
			bd->inserirConfiguracao(*conf);
			break;
		case 3:
			//eliminar configuracao robo (base de dados)
			do
			{
				cout << "Introduza o codigo da configuracao que pretende eliminar: " << endl;
				cin >> codConfig;
				cout << "Tem a certeza que pretende eliminar a configuracao com o codigo: " + codConfig + " ?" << endl;
				bd->mostrarConfiguracao(codConfig);
				cout << "S -Sim" << endl << "N -Nao" << endl;
				cin >> resp;
				if (toupper(resp) != 'S' || toupper(resp) != 'N')
					cout << "Opcao invalida!";
			} while (toupper(resp) != 'S');

			bd->eliminarRobo(codConfig);
			break;
		case 4:
			menuPrincipal();
			break;
		default:
			cout << "Opcao invalida! Insira nova opcao!" << endl;
			break;

		}
	} while (opcConfiguracao != 4);
}

#ifndef Configuracao_
#define Configuracao_

#include <string>
using namespace std;

class Configuracao
{
private:
	string sensor;
	int velocidade;
	string funcoes;

public:
	Configuracao();
	Configuracao(const Configuracao &con);
	Configuracao(string sensores, int veloc, string functions);
	void setSensor(string sensores);
	void setVelocidade(int veloc);
	void setFuncoes(string functions);
	string getSensor() const;
	int getVelocidade() const;
	string getFuncoes() const;

};
#endif

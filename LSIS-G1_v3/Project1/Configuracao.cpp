#include "Configuracao.h"

Configuracao::Configuracao()
{

}
Configuracao::Configuracao(const Configuracao &con)
{
	sensor = con.sensor;
	velocidade = con.velocidade;
	funcoes = con.velocidade;
}
Configuracao::Configuracao(string sensores, int veloc, string functions) :
	sensor(sensores), velocidade(veloc), funcoes(functions)
{

}
void Configuracao::setSensor(string sensores)
{
	sensor = sensores;
}
void Configuracao::setVelocidade(int veloc)
{
	velocidade = veloc;
}
void Configuracao::setFuncoes(string functions)
{
	funcoes = functions;
}
string Configuracao::getSensor() const
{
	return sensor;
}
int Configuracao::getVelocidade() const
{
	return velocidade;
}
string Configuracao::getFuncoes() const
{
	return funcoes;
}

#include <iostream>
using namespace std;

#include "Menu.h"
#include "Equipa.h"

void Menu::menuPrincipal()
{
	int opc;
	cout << "******************** MENU PRINCIPAL ****************" << endl;
	cout << "1) Equipa" << endl << "2) Robo" << endl << "3) Prova" << endl << "4) Codigo" << endl << "5) Configuracao" << endl << "6) Creditos" << endl << "****************************************************" << endl;
	cin >> opc;
	switch (opc)
	{
	case '1':
		//inserir menu equipa
		menuEquipa();
		break;
	case '2':
		//inserir menu robo
		menuRobo();
		break;
	case '3':
		//inserir menu prova
		menuProva();
		break;
	case '4':
		//inserir menu codigo
		menuCodigo();
		break;
	case '5':
		//inserir menu configuracao
		menuConfiguracao();
		break;
	case '6':
		cout << "No ambito da disciplina Laboratorios de Sistemas 1 foi proposto a realiza��o de um robo bombeiro e de um sistema de informacao para gerir toda a informacao para uma competicao deste tipo." << endl;
		cout << "Trabalho realizado por: " << endl << "Ana Nogueira - 1140476" << endl << "Daniel Barbosa - 1141251" << endl << "Diogo Monteiro - 1140544" << endl << "Pedro Ribeiro - 1141188" << endl;
		cout << "Docentes: " << endl << "Andr� Dias" << endl << "Alberto Sampaio" << endl;
		break;
	default:
		cout << "Opcao invalida! Insira novamente a opcao . ";
		break;
	}
}

void Menu::menuEquipa()
{
	int opcEquipa, numEquipa = 1;
	Equipa equi;

	cout << "********************MENU EQUIPA****************" << endl;
	cout << "1) Informacao equipa" << endl << "2) Inserir equipa" << endl << "3) Eliminar equipa" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
	cin >> opcEquipa;
	switch (opcEquipa)
	{
	case '1':
		//lerEquipa da base de dados

		break;
	case '2':
		//inserir equipa (base de dados)

		equi.InserirEquipa(numEquipa);

		break;
	case '3':
		//eliminar equipa (base de dados)
		break;
	case '4':
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

	}
}

void Menu::menuRobo()
{
	int opcRobo;
	cout << "********************MENU ROBO****************" << endl;
	cout << "1) Informacao robo" << endl << "2) Inserir robo" << endl << "3) Eliminar robo" << endl << "4) Retornar menu principal" << endl << "*********************************************" << endl;
	cin >> opcRobo;
	switch (opcRobo)
	{
	case '1':
		//lerRobo da base de dados
		break;
	case '2':
		//inserir robo (base de dados)
		break;
	case '3':
		//eliminar robo (base de dados)
		break;
	case '4':
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

	}
}

void Menu::menuProva()
{
	int opcProva;
	cout << "********************MENU PROVA****************" << endl;
	cout << "1) Informacao prova" << endl << "2) Inserir prova" << endl << "3) Eliminar prova" << endl << "4) Retornar menu principal" << endl << "**********************************************" << endl;
	cin >> opcProva;
	switch (opcProva)
	{
	case '1':
		//lerProva da base de dados
		break;
	case '2':
		//inserir prova (base de dados)
		break;
	case '3':
		//eliminar prova (base de dados)
		break;
	case '4':
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

	}
}

void Menu::menuCodigo()
{
	int opcCodigo;
	cout << "********************MENU CODIGO****************" << endl;
	cout << "1) Informacao codigo robo" << endl << "2) Inserir codigo robo" << endl << "3) Eliminar codigo robo" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
	cin >> opcCodigo;
	switch (opcCodigo)
	{
	case '1':
		//lerCodigo da base de dados
		break;
	case '2':
		//inserir codigo robo (base de dados)
		break;
	case '3':
		//eliminar codigo robo (base de dados)
		break;
	case '4':
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

	}
}

void Menu::menuConfiguracao()
{
	int opcConfiguracao;
	cout << "********************MENU CONFIGURACAO****************" << endl;
	cout << "1) Informacao configuracao" << endl << "2) Inserir configuracao" << endl << "3) Eliminar configuracao" << endl << "4) Retornar menu principal" << endl << "*****************************************************" << endl;
	cin >> opcConfiguracao;
	switch (opcConfiguracao)
	{
	case '1':
		//lerConfiguracao da base de dados
		break;
	case '2':
		//inserir configuracao robo (base de dados)
		break;
	case '3':
		//eliminar configuracao robo (base de dados)
		break;
	case '4':
		menuPrincipal();
		break;
	default:
		cout << "Opcao invalida! Insira nova opcao!" << endl;
		break;

	}
}
#ifndef Prova_
#define Prova_

#include <string>
using namespace std;

class Prova
{
private:
	int data;
	int hora;
	string local;
	string tempo;
	string posicao;
	
public:
	Prova();
	Prova(const Prova &pr);
	Prova(int date, int hour, string localP, string time, string posicionamento);
	void setData(int date);
	void setHora(int hour);
	void setLocal(string localP);
	void setTempo(string time);
	void setPosicao(string posicionamento);
	int getData() const;
	int getHora() const;
	string getLocal() const;
	string getTempo() const;
	string getPosicao() const;

};

#endif

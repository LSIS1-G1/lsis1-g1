#ifndef Codigo_
#define Codigo_

#include <string>
using namespace std;

class Codigo
{
private:
	string codigo;
	string funcao;

public:
	Codigo();
	Codigo(const Codigo &c);
	Codigo(string code, string function);
	void setCodigo(string code);
	void setFuncao(string function);
	string getCodigo() const;
	string getFuncao() const;

};
#endif // !Codigo_

#ifndef Menu_
#define Menu_


//#include "Equipa.h"

class Menu
{
	private:
		int opc;
	public:
		void menuPrincipal();
		void menuEquipa();
		void menuRobo();
		void menuCodigo();
		void menuConfiguracao();
		void menuProva();
};
#endif

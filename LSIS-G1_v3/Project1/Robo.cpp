#include "Robo.h"

Robo::Robo()
{

}
Robo::Robo(const Robo &r)
{
	nome = r.nome;
	codigo = r.codigo;
	equipa = r.equipa;
	cor = r.cor;
}
Robo::Robo(string name, string code, string equi, string color) :
	nome(name), codigo(code), equipa(equi), cor(color)
{

}

void Robo::setNome(string name)
{
	nome = name;
}
void Robo::setCodigo(string code)
{
	codigo = code;
}
void Robo::setEquipa(string equi)
{
	equipa = equi;
}
void Robo::setCor(string color)
{
	cor = color;
}
string Robo::getNome() const
{
	return nome;
}
string Robo::getCodigo() const
{
	return codigo;
}
string Robo::getEquipa() const
{
	return equipa;
}
string Robo::getCor() const
{
	return cor;
}
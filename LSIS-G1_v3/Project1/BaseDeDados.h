#ifndef BaseDados_
#define BaseDados_


#include <iostream>
#include <string>
#include <exception>

#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/sqlstring.h>


using namespace std;

#include "Equipa.h"

using namespace sql;


class BaseDados {
private:

	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
	string nomeBD;

public:
	BaseDados();
	~BaseDados();
	//BaseDados(string nome);

	void ligar();
	bool lerEquipa(int numEqui, Equipa & equipa);
	void inserir(Equipa &eq);

};
#endif

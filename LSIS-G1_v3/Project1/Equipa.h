/**
/* Estas duas instrucoes evitam que a classe seja incluida mais do que uma vez
*/
#ifndef Equipa_
#define Equipa_



#include <string>
#include <iostream>
using namespace std;

//A classe Equipa tera os atributos correspondentes a tabela Equipa da base de dados
class Equipa
{

private:
	
	string NomeEquipa;
	string nomeElem;
	string lema;
	string idade; 
	int numElementos; //numero de elementos da equipa
	string Localidade;
	int numEquipa;

public:
	Equipa();
	Equipa(const Equipa &eq);
	Equipa(string nomeEq, string nomeEl, string Lema, string age, int numEl, string local, int numEqui);
	void setNomeEq(string nomeEq);
	void setNomeEl(string nomeEle);
	void setLema(string lemaEq);
	void setIdade(string idade);
	void setNumElementos(int numEle);
	void setLocalidade(string local);
	void setNumEquipa(int numEqui);
	string getNomeEq() const;
	string getNomeEle() const;
	string getLema() const;
	string getIdade() const;
	int getNumElementos() const;
	string getLocalidade() const;
	int getNumEquipa() const;
	Equipa InserirEquipa(int numEquipa);
};

#endif
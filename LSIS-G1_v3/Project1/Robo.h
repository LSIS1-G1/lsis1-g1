#ifndef Robo_
#define Robo_

#include <string>
using namespace std;

class Robo
{
	private:
		string nome;
		string codigo;
		string equipa;
		string cor;

	public:
		Robo();
		Robo(const Robo &r);
		Robo(string name, string code, string equi, string color);
		void setNome(string name);
		void setCodigo(string code);
		void setEquipa(string equi);
		void setCor(string color);
		string getNome() const;
		string getCodigo() const;
		string getEquipa() const;
		string getCor() const;
};
#endif

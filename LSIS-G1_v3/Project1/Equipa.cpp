#include "Equipa.h"

Equipa::Equipa()
{

}

Equipa::Equipa(const Equipa &eq)
{
	NomeEquipa = eq.NomeEquipa;
	nomeElem = eq.nomeElem;
	lema = eq.lema;
	idade = eq.idade;
	numElementos = eq.numElementos;
	Localidade = eq.Localidade;
	numEquipa = eq.numEquipa;

}

Equipa::Equipa(string nomeEq, string nomeEl, string Lema, string age, int numEl, string local, int numEqui) :
	NomeEquipa(nomeEq), nomeElem(nomeEl), lema(Lema), idade(age), numElementos(numEl), Localidade(local), numEquipa(numEqui)
{
	numEquipa++;
}

void Equipa::setNomeEq(string nomeEq)
{
	NomeEquipa = nomeEq;
}

void Equipa::setNomeEl(string nomeEl)
{
	nomeElem = nomeEl;
}

void Equipa::setIdade(string age)
{
	idade = age;
}

void Equipa::setNumElementos(int numEl)
{
	numElementos = numEl;
}

void Equipa::setLema(string Lema)
{
	lema = Lema;
}

void Equipa::setLocalidade(string local)
{
	Localidade = local;
}
void Equipa::setNumEquipa(int numEqui)
{
	numEquipa = numEqui;
}

string Equipa::getNomeEq() const
{
	return NomeEquipa;
}
string Equipa::getNomeEle() const
{
	return nomeElem;
}
string Equipa::getLema()const
{
	return lema;
}
string Equipa::getIdade() const
{
	return idade;
}
int Equipa::getNumElementos() const
{
	return numElementos;
}
string Equipa::getLocalidade()const
{
	return Localidade;
}
int Equipa::getNumEquipa() const
{
	return numEquipa;
}


//////////////////////////////////////////////////VERS�O ANA JULIA:
Equipa Equipa::InserirEquipa(int numEquipa)
{
	cout << "Introduza o nome da equipa: " << endl;
	cin >> NomeEquipa;
	cout << "Introduza o numero de elementos: " << endl;
	cin >> numElementos;

	cout << "Introduza os nomes dos elementos da equipa: " << endl;
	cin >> nomeElem;
	cout << "Introduza as idades dos elementos, respetivamente: " << endl;
	cin >> idade;

	cout << "Introduza a localidade: " << endl;
	cin >> Localidade;
	cout << "Introduza o lema da equipa: " << endl;
	cin >> lema;

	return Equipa(NomeEquipa, nomeElem, lema, idade, numElementos, Localidade, numEquipa);
}
#ifndef Menu_
#define Menu_


//#include "Equipa.h"

class Menu
{
	private:
		int opc;
	public:
		void menuPrincipal();
		void menuEquipa();
		void menuRobo();
		void menuJogadores();
		void menuConfiguracao();
		void menuProva();
		char lerOpcao();
		void ligacaoArduino();
};
#endif

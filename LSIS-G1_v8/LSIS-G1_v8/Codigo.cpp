#include "Codigo.h"


Codigo::Codigo()
{

}

Codigo::Codigo(const Codigo &c)
{
	codigo = c.codigo;
	funcao = c.funcao;

}
Codigo::Codigo(string code, string function) :
	codigo(code), funcao(function)
{

}

void Codigo::setCodigo(string code)
{
	codigo = code;
}
void Codigo::setFuncao(string function)
{
	funcao = function;
}
string Codigo::getCodigo() const
{
	return codigo;
}
string Codigo::getFuncao() const
{
	return funcao;
}
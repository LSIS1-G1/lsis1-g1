#include "Menu.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <tchar.h>

using namespace std;


#include "BaseDeDados.h"
#include "utils.h"
#include "tSerial.h"
#include "Arduino.h"

#include "Equipa.h"
#include "Robo.h"
#include "Configuracao.h"
#include "Jogador.h"
#include "Prova.h"

/*Menus para cada uma das Classes: */
#include "MenuEquipa.h"
#include "MenuConfiguracao.h"
#include "MenuRobo.h"
#include "MenuJogador.h"
#include "MenuProva.h"


char Menu::lerOpcao() {
	char opcao = '0';
	cin >> opcao;
	return opcao;
}

void Menu::menuPrincipal()
{
	Arduino *a = new Arduino();
	char opc;
	do
	{
		cout << endl;
		cout << "******************** MENU PRINCIPAL ****************" << endl;
		cout << "1) Equipa" << endl << "2) Robo" << endl << "3) Prova" << endl << "4) Jogadores" << endl << "5) Configuracao" << endl << "6) Ligacao Arduino" << endl << "7) Creditos "<<endl<< "8) Encerrar o programa" << endl << "****************************************************" << endl;
		opc = lerOpcao();

		switch (opc)
		{  
		case '1':
			//inserir menu equipa
			menuEquipa();
			break;
		case '2':
			//inserir menu robo
			menuRobo();
			break;
		case '3':
			//inserir menu prova
			menuProva();
			break;
		case '4':
			//inserir menu jogadores
			menuJogadores();
			break;
		case '5':
			//inserir menu configuracao
			menuConfiguracao();
			break;
		case '6':
			a->ligacaoArduino();
			break;
		case '7':
			cout << "****************************************************" << endl;
			cout << "*********************** CREDITOS *******************" << endl;
			cout << "No ambito da disciplina Laboratorios de Sistemas 1, da Licenciatura em Engenharia de Sistemas, foi proposto a realizacao de um robo bombeiro e de um sistema de informacao para gerir toda a informacao para uma competicao deste tipo." << endl;
			cout << "Docentes: " << endl << "Andre Dias" << endl << "Alberto Sampaio" << endl;
			cout << "2015/2016" << endl;
			cout << "Trabalho realizado por: " << endl << "Ana Nogueira - 1140476" << endl << "Daniel Barbosa - 1141251" << endl << "Diogo Monteiro - 1140544" << endl << "Pedro Ribeiro - 1141188" << endl;
			cout << "****************************************************" << endl;
			break;
		case '8': //Encerramento do programa
			exit(1);
			break;

		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. " << endl;
			break;
		}
	} while (opc != 8);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuEquipa()
{
	char opcEquipa;
	int numEquipa = 1, nEquipa = 0;
	string NomeEquipa, lema, Localidade;
	Equipa* equi = new Equipa();
	BaseDados* bd = new BaseDados();
	MenuEquipa *mE = new MenuEquipa();
	do {

		cout << endl << "********************MENU EQUIPA****************" << endl;
		cout << "1) Informacao equipa" << endl << "2) Inserir equipa" << endl << "3) Eliminar equipa" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
		opcEquipa = lerOpcao();
		char opcEq;
		switch (opcEquipa)
		{
		case '1':
			mE->informacaoEquipa();

			break;
		case '2':
			//inserir equipa (base de dados)
				cout << "Introduza o numero da equipa: " << endl;
				cin >> nEquipa;
				if ((bd->lerEquipa(nEquipa)) == true)
				{
					while ((bd->lerEquipa(nEquipa)) == true)
					{
						cout << "Insira outro n�mero: ";
						cin >> nEquipa;
					}
				}
				else
				{
					equi->preencherEquipa(nEquipa);
					bd->inserirEquipa(*equi);
				}
			break;
		case '3':
				//eliminar equipa (base de dados)
				cout << "Indique o numero de equipa que pretende eliminar: ";
				cin >> nEquipa;
				bd->eliminarEquipa(nEquipa);
			break;
		case '4':
				//retornar ao menu principal;
				menuPrincipal();
				break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcEquipa != 4);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuRobo()
{
	char opcRobo;
	string codeRobo;
	Robo *robot = new Robo();
	BaseDados *bd = new BaseDados();
	MenuRobo *mR = new MenuRobo();
	do {
		cout << endl << "********************MENU ROBO****************" << endl;
		cout << "1) Informacao robo" << endl << "2) Inserir robo" << endl << "3) Eliminar robo" << endl << "4) Retornar menu principal" << endl << "*********************************************" << endl;
		opcRobo = lerOpcao();
		switch (opcRobo)
		{
		case '1': 
			//Informacao Robo
			mR->informacaoRobo();
			break;
		case '2':
			//inserir robo (base de dados)
			cout << "Introduza o codigo do robo: " << endl;
			cin >> codeRobo;
			if ((bd->lerRobo(codeRobo)) == true)
			{
				while ((bd->lerRobo(codeRobo)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codeRobo;
				}
			}
			else
			{
				robot->preencherRobo(codeRobo);
				bd->inserirRobo(*robot);
			}
			break;
		case '3':
			//eliminar robo (base de dados)
			cout << "Indique o numero do robo que pretende eliminar: ";
			cin >> codeRobo;
			bd->eliminarRobo(codeRobo);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0': 
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcRobo != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuProva()
{
	BaseDados *bd = new BaseDados();
	Prova *prova = new Prova();
	MenuProva *mP = new MenuProva();
	string codProva;
	char opcProv;
	do
	{
		cout << endl << "********************MENU PROVA****************" << endl;
		cout << "1) Informacao prova" << endl << "2) Inserir prova" << endl << "3) Eliminar prova" << endl << "4) Retornar menu principal" << endl << "**********************************************" << endl;
		opcProv = lerOpcao();
		char opcP;
		switch (opcProv)
		{
		case '1':
			//lerProva da base de dados
			mP->informacaoProva();
			break;
		case '2':
			//inserir prova (base de dados)
			cout << "Introduza o codigo da prova: " << endl;
			cin >> codProva;
			if ((bd->lerProva(codProva)) == true)
			{
				while ((bd->lerProva(codProva)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codProva;
				}
			}
			else
			{
				prova->preencherProva(codProva);
				bd->inserirProva(*prova);
			}
			break;
		case '3':
			//eliminar prova (base de dados)
			cout << "Indique o numero da prova que pretende eliminar: ";
			cin >> codProva;
			bd->eliminarProva(codProva);
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcProv != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void Menu::menuJogadores()
{
	BaseDados *bd = new BaseDados();
	Jogador *jog = new Jogador();
	MenuJogador *mJog = new MenuJogador();
	int numJog;
	char opcM;
	
	do
	{
		cout << endl << "********************MENU JOGADOR****************" << endl;
		cout << "1) Informacao do jogador" << endl << "2) Inserir jogador" << endl << "3) Eliminar jogador" << endl << "4) Retornar menu principal" << endl << "***********************************************" << endl;
		opcM = lerOpcao();
		char opcJo;
		switch (opcM)
		{
		case '1':
			//lerJogador da base de dados
			mJog->informacaoJogadores();
			break;
		case '2':
			//inserir jogador (base de dados)
			cout << "Introduza o numero do jogador: " << endl;
			cin >> numJog;
			if ((bd->lerJogador(numJog)) == true)
			{
				while ((bd->lerJogador(numJog)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> numJog;
				}
			}
			else
			{
				jog->preencherJogador(numJog);
				bd->inserirJogador(*jog);
			}
			
			break;
		case '3':
			//eliminar jogador robo (base de dados)
				cout << "Introduza o numero do jogador que pretende eliminar: " << endl;
				cin >> numJog;
				bd->eliminarJogador(numJog);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcM != 4);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Menu::menuConfiguracao()
{
	char opcConfiguracao;
	BaseDados *bd = new BaseDados();
	Configuracao *conf = new Configuracao();
	string codConfig;
	MenuConfiguracao *mC = new MenuConfiguracao();
	do
	{
		cout << endl << "********************MENU CONFIGURACAO****************" << endl;
		cout << "1) Informacao configuracao" << endl << "2) Inserir configuracao" << endl << "3) Eliminar configuracao" << endl << "4) Retornar menu principal" << endl << "*****************************************************" << endl;
		opcConfiguracao = lerOpcao();
		char opcC;
		switch (opcConfiguracao)
		{
		case '1':
			//lerConfiguracao da base de dados
			mC->informacaoConfiguracao();
		break;
		case '2':
			//inserir configuracao robo (base de dados)
			cout << "Introduza o codigo da configuracao: " << endl;
			cin >> codConfig;
			if ((bd->lerConfiguracao(codConfig)) == true)
			{
				while ((bd->lerConfiguracao(codConfig)) == true)
				{
					cout << "Insira outro n�mero: ";
					cin >> codConfig;
				}
			}
			else
			{ 
				conf->preencherConfiguracao(codConfig);
				bd->inserirConfiguracao(*conf);
			}
			break;
		case '3':
			//eliminar configuracao robo (base de dados)
			cout << "Introduza o codigo da configuracao que pretende eliminar: " << endl;
			cin >> codConfig;
			bd->eliminarConfiguracao(codConfig);
			break;
		case '4':
			menuPrincipal();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcConfiguracao != 4);
}

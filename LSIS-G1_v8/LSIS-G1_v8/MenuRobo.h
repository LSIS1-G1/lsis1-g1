#ifndef MenuRobo_
#define MenuRobo_

#include <iostream>
#include <string>

using namespace std;

class MenuRobo
{

private:

	string codigo;
	int numEquipa;
	string nome;
	string cor;
	string funcaoP;

public:
	MenuRobo();

	void setNome(string nameTeam);
	void setEquipa(int numTeam);
	void setCodigo(string codigoRobo);
	void setCor(string corR);
	void setFuncaoP(string funcaoPri);

	string getNomeEquipa()const;
	int getNumeroEquipa()const;
	string getCodigo()const;
	string getCor() const;
	string getFuncaoP() const;


	void informacaoRobo();

};

#endif

#include "MenuEquipa.h"
#include <iostream>
#include <string>

#include "BaseDeDados.h"
#include "Menu.h"


 MenuEquipa:: MenuEquipa()
{

}

 void MenuEquipa::setNome(string nameTeam)
{
	nomeEquipa = nameTeam;
}

void MenuEquipa::setNumeroEquipa(int numTeam)
{
	numEquipa = numTeam;
}

void MenuEquipa::setLema(string Lema)
{
	lema = Lema;
}

void MenuEquipa::setLocalidade(string local)
{
	localidade = local;
}

string MenuEquipa:: getNomeEquipa() const
{
	return nomeEquipa;
}

int MenuEquipa::getNumeroEquipa() const
{
	return numEquipa;
}

string MenuEquipa::getLema() const
{
	return lema;
}

string MenuEquipa::getLocalidade() const
{
	return localidade;
}

void MenuEquipa:: informacaoEquipa()
{
	char opcEq;
	BaseDados* bd = new BaseDados();
	Menu *mEqui = new Menu();
	do
	{
		int codeTeam;
		cout << "***************INFORMACAO DA EQUIPA***************" << endl;
		cout << "1) Listar equipas " << endl << "2) Visualizar uma equipa" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu equipa" << endl << "***********************************************" << endl;
		opcEq = mEqui->lerOpcao();
		switch (opcEq)
		{
		case '1':
			//listar equipas
			bd->listarEquipas();
			break;
		case '2':
			//visualizar toda a informacao da equipa
			cout << "Insira o numero da equipa que pretende visualizar: ";
			cin >> codeTeam;
			if ((bd->lerEquipa(codeTeam)) == true)
			{
				bd->mostrarEquipa(codeTeam);
			}
			else
			{
				while ((bd->lerEquipa(codeTeam)) == false)
				{
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeTeam;

				}
			}
			break;
		case '3':
			//atualizar informacao
			//primeiro tem de saber qual a equipa que pretende
			cout << "Introduza o numero da equipa que pretende alterar: " << endl;
			cin >> codeTeam;
			if ((bd->lerEquipa(codeTeam)) == true)
			{
				bd->atualizarEquipas(codeTeam);
			}
			else
			{
				while ((bd->lerEquipa(codeTeam)) == false)
				{
					cout << "Insira o numero da equipa que pretende alterar: ";
					cin >> codeTeam;

				}
			}
			break;
		case '4':
			//retorna ao menu equipa
			mEqui->menuEquipa();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente uma opcao viavel. " << endl;
			break;
		}

	} while (opcEq != 4);
}
#ifndef MenuProva_
#define MenuProva_

#include <iostream>
#include <string>

using namespace std;

class MenuProva
{

private:

	string codigo;
	int numEquipa;
	string DataProva;
	string HoraProva;
	string LocalProva;
	int TempoProva;
	int PosicionamentoProva;
	

public:
	MenuProva();

	void setCodigo(string codigo);
	void setNumEquipa(int numTeam);
	void setDataProva(string data);
	void setHoraProva(string hora);
	void setLocalProva(string local);
	void setTempoProva(int tempo);
	void setPosicionamentoProva(int posicao);

	string getCodigo()const;
	int getNumeroEquipa()const;
	string getDataProva()const;
	string getHoraProva()const;
	string getLocalProva()const;
	int getTempoProva()const;
	int getPosicionamentoProva()const;


	void informacaoProva();

};

#endif
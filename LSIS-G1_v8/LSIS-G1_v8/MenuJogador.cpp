#include "MenuJogador.h"
#include "BaseDeDados.h"
#include "Menu.h"

#include <iostream>
#include <string>


MenuJogador::MenuJogador()
{

}

void MenuJogador::setNome(string nomeJogador)
{
	nome = nomeJogador;
}

void MenuJogador::setIdade(int idadeJogador)
{
	idade = idadeJogador;
}

void MenuJogador::setNumEquipa(int numEquipa)
{
	numeroequipa = numEquipa;
}

void MenuJogador::setNumJogador(int numJogador)
{
	numerojogador = numJogador;
}

string MenuJogador::getNomeJogador() const
{
	return nome;
}

int MenuJogador::getIdade() const
{
	return idade;
}

int MenuJogador::getNumEquipa() const
{
	return numeroequipa;
}

int MenuJogador::getNumJogador() const
{
	return numerojogador;
}

void MenuJogador::informacaoJogadores()
{
	char opcJo;
	BaseDados* bd = new BaseDados();
	Menu *mJog = new Menu();
	do
	{
		int numJ;
		cout << endl << "***************INFORMACAO DO JOGADOR***************" << endl;
		cout << "1) Listar jogador " << endl << "2) Visualizar informacao de uma jogador" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu jogador" << endl << "*********************************************" << endl;
		opcJo = mJog->lerOpcao();
		switch (opcJo)
		{
		case '1': //Listar jogadores
			bd->listarJogadores();
			break;

		case '2': //Visualizar um jogador
			cout << "Introduza o numero do jogador que pretende visualizar: " << endl;
			cin >> numJ;
			if ((bd->lerJogador(numJ)) == true)
			{
				bd->mostrarJogador(numJ);
			}
			else
			{
				while ((bd->lerJogador(numJ)) == false)
				{
					cout << "Insira o numero do jogador que pretende visualizar: ";
					cin >> numJ;
				}
			}
			bd->mostrarJogador(numJ);
			break;

		case '3': //Atualizar informacao
				  //primeiro e necessario saber qual a prova que se pretende atualizar
			cout << "Introduza o numero do jogador que pretende atualizar informacao: " << endl;
			cin >> numJ;
			if ((bd->lerJogador(numJ)) == true)
			{
				bd->atualizarJogador(numJ);
			}
			else
			{
				while ((bd->lerJogador(numJ)) == false)
				{
					cout << "Insira o numero da equipa que pretende atualizar: ";
					cin >> numJ;
				}
			}
			break;

		case '4': //torna ao menu jogadores
			mJog->menuJogadores();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;;
		}
	} while (opcJo != 4);
}
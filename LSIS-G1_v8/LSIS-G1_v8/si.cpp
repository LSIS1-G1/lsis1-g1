/*
	Licenciatura em Engenharia de Sistemas; Instituto Superior de Engenharia do Porto; 
	Disciplina: Laboratórios de Sistemas 1;
	Ano Letivo: 2015/2016;
	Turma: 2DB;
	Docentes: André Dias; Alberto Sampaio;

	Trabalho realizado por:
	Ana Nogueira - 1140476
	Daniel Barbosa - 1141251
	Diogo Monteiro - 1140544
	Pedro Ribeiro - 1141188
*/
#include <iostream>
using namespace std;

#include <iostream>
#include "Equipa.h"
#include "Codigo.h"
#include "Prova.h"
#include "Robo.h"
#include "Configuracao.h"
#include "BaseDeDados.h"
#include "Menu.h"
#include "Arduino.h"
#include "BaseDeDados.h"
#include "Registo.h"



void login(Registo &reg)
{
	Menu *m = new Menu();
//	Registo *reg = new Registo();
	string pw;
	BaseDados *bd = new BaseDados();
	cout << endl << "______________________________________________________" << endl;
	cout << "| ---------------------- Login: --------------------- |" << endl;
	cout << "| Password: ";
	cin >> pw;
	if (bd->pwExiste(pw) == true)
	{
		cout << endl << "Bem-Vindo "+reg.getNome()+"!";
		m->menuPrincipal();
	}
	else
	{
		cout << "Password: ";
		cin >> pw;
		while (bd->pwExiste(pw) == false)
		{
			cout << "Password: ";
			cin >> pw;
		}
	}
}


int main()
{
	Registo *reg = new Registo();
	BaseDados *bd = new BaseDados();

	
	//cout << "Arranque..." << endl;
	int numID = 0;
	do {
		cout << "ID do utilizador: ";
		cin >> numID;
	} while (numID <= 0);

	if (bd->idExiste(numID) == true)
		login(*reg);
	 if(bd->idExiste(numID) == false)
	{
		reg->preencherRegisto(numID);
		bd->inserirRegisto(*reg);
		login(*reg);
	}
}
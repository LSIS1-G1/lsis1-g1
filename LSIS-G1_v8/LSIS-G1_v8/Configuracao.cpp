#include "Configuracao.h"
#include <iostream>

Configuracao::Configuracao()
{

}
Configuracao::Configuracao(const Configuracao &con)
{
	sensor = con.sensor;
	velocidade = con.velocidade;
	funcoes = con.velocidade;
	codigoR = con.codigoR;
}
Configuracao::Configuracao(string codigo, string sensores, int veloc, string functions) :
	codigoR(codigo),sensor(sensores), velocidade(veloc), funcoes(functions)
{

}
void Configuracao::setSensor(string sensores)
{
	sensor = sensores;
}
void Configuracao::setVelocidade(int veloc)
{
	velocidade = veloc;
}
void Configuracao::setFuncoes(string functions)
{
	funcoes = functions;
}
void Configuracao::setCodigo(string codigo)
{
	codigoR = codigo;
}
string Configuracao::getSensor() const
{
	return sensor;
}
int Configuracao::getVelocidade() const
{
	return velocidade;
}
string Configuracao::getFuncoes() const
{
	return funcoes;
}
string Configuracao::getCodigo() const
{
	return codigoR;
}

void Configuracao::preencherConfiguracao(string codigo)
{
	Configuracao *conf = new Configuracao();


	cout << "Introduza os sensores que o robo possui: " << endl;
	cin >> sensor;
	cout << "Introduza a velocidade maxima que o robo atinge: " << endl;
	cin >> velocidade;
	cout << "Introduza a funcao extra do robo: " << endl;
	cin >> funcoes;
	
	codigoR = codigo;


}

#ifndef MenuEquipa_
#define MenuEquipa_

#include <iostream>
#include <string>

using namespace std;

class MenuEquipa
{

private:

	string nomeEquipa;
	int numEquipa;
	string lema;
	string localidade;

public:
	MenuEquipa();

	void setNome(string nameTeam);
	void setNumeroEquipa(int numTeam);
	void setLema(string lema);
	void setLocalidade(string local);

	string getNomeEquipa()const;
	int getNumeroEquipa()const;
	string getLema()const;
	string getLocalidade() const;


	void informacaoEquipa();

};

#endif

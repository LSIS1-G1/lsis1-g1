#ifndef MenuJogador_
#define MenuJogador_

#include <iostream>
#include <string>

using namespace std;

class MenuJogador
{

private:

	string nome;
	int numeroequipa;
	int idade;
	int numerojogador;

public:
	MenuJogador();

	void setNome(string nameJogador);
	void setNumJogador(int NumJogador);
	void setNumEquipa(int NumEquipa);
	void setIdade(int idadeJogador);

	string getNomeJogador()const;
	int getNumJogador()const;
	int getNumEquipa()const;
	int getIdade() const;
	
	void informacaoJogadores();
};

#endif


#include "MenuRobo.h"
#include "Menu.h"
#include "BaseDeDados.h"

#include <iostream>
#include <string>


MenuRobo::MenuRobo()
{

}

void MenuRobo::setNome(string nameTeam)
{
	nome = nameTeam;
}

void MenuRobo::setEquipa(int numTeam)
{
	numEquipa = numTeam;
}

void MenuRobo::setCodigo(string codigoRobo)
{
	codigo = codigoRobo;
}

void MenuRobo::setCor(string corR)
{
	cor = corR;
}

void MenuRobo::setFuncaoP(string funcaoPri)
{
	funcaoP = funcaoPri;
}


string MenuRobo::getNomeEquipa() const
{
	return nome;
}

int MenuRobo::getNumeroEquipa() const
{
	return numEquipa;
}

string MenuRobo::getCodigo() const
{
	return codigo;
}

string MenuRobo::getCor() const
{
	return cor;
}

string MenuRobo::getFuncaoP() const
{
	return funcaoP;
}

void MenuRobo::informacaoRobo()
{
	BaseDados* bd = new BaseDados();
	Menu *mRobo = new Menu();
	char opcR;
	do
	{
		string codeRobot;
		cout << "***************INFORMACAO DO ROBO***************" << endl;
		cout << "1) Listar robos " << endl << "2) Visualizar um robo" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu robo" << endl << "*********************************************" << endl;
		opcR = mRobo->lerOpcao();
		switch (opcR)
		{
		case '1':
			//Listar Robos
			bd->listarRobo();
			break;

		case '2':
			//Visualizar um robo
			cout << "Introduza o codigo do robo que pretende visualizar: " << endl;
			cin >> codeRobot;
			if ((bd->lerRobo(codeRobot)) == true)
			{
				bd->mostrarRobo(codeRobot);
			}
			else
			{
				while ((bd->lerRobo(codeRobot)) == false)
				{
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeRobot;
				}
			}
			break;

		case '3':
			//Atualizar informacao
			//primeiro e necessario saber qual o robo
			cout << "Introduza o codigo do robo que pretende atualizar informacao: " << endl;
			cin >> codeRobot;
			if ((bd->lerRobo(codeRobot)) == true)
			{
				bd->atualizarRobo(codeRobot);
			}
			else
			{
				while ((bd->lerRobo(codeRobot)) == false)
				{
					cout << "Insira o numero do robo que pretende atualizar: ";
					cin >> codeRobot;
				}
			}
			break;
		case '4': //Retornar ao menu robo
			mRobo->menuRobo();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}

	} while (opcR != 4);
}
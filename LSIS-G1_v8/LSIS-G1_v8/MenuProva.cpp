
#include "MenuProva.h"
#include "Menu.h"
#include "BaseDeDados.h"

MenuProva::MenuProva()
{
}

void MenuProva::setCodigo(string code)
{
	codigo = code;
}

void MenuProva::setNumEquipa(int num)
{
	numEquipa = num;
}

void MenuProva::setDataProva(string data)
{
	DataProva = data;
}

void MenuProva::setHoraProva(string hora)
{
	HoraProva = hora;
}

void MenuProva::setLocalProva(string local)
{
	LocalProva = local;
}

void MenuProva::setTempoProva(int tempo)
{
	TempoProva = tempo;
}

void MenuProva::setPosicionamentoProva(int pos)
{
	PosicionamentoProva = pos;
}

string MenuProva::getCodigo() const
{
	return codigo;
}

int MenuProva::getNumeroEquipa() const
{
	return numEquipa;
}

string MenuProva::getDataProva() const
{
	return DataProva;
}

string MenuProva::getHoraProva() const
{
	return HoraProva;
}

string MenuProva::getLocalProva() const
{
	return LocalProva;
}

int MenuProva::getTempoProva() const
{
	return TempoProva;
}

int MenuProva::getPosicionamentoProva() const
{
	return PosicionamentoProva;
}

void MenuProva::informacaoProva()
{	
	char opcP;
	string codeProva;
	do
	{
		BaseDados* bd = new BaseDados();
		Menu *mProva = new Menu();
		cout << endl << "***************INFORMACAO DA PROVA***************" << endl;
		cout << "1) Listar provas " << endl << "2) Visualizar informacao de uma prova" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu provas" << endl << "*********************************************" << endl;
		opcP = mProva->lerOpcao();
		switch (opcP)
		{
		case '1': //Listar Prova
			bd->listarProvas();
			break;

		case '2': //Visualizar uma prova
			cout << "Introduza o codigo da prova que pretende visualizar: " << endl;
			cin >> codeProva;
			if ((bd->lerProva(codeProva)) == true)
			{
				bd->mostrarProva(codeProva);
			}
			else
			{
				while ((bd->lerProva(codeProva)) == false)
				{
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeProva;
				}
			}
			break;

		case '3':
			//Atualizar informacao
			//primeiro e necessario saber qual a prova que se pretende atualizar
			cout << "Introduza o codigo da prova que pretende atualizar informacao: " << endl;
			cin >> codeProva;
			if ((bd->lerProva(codeProva)) == true)
			{
				bd->atualizarProva(codeProva);
			}
			else
			{
				while ((bd->lerProva(codeProva)) == false)
				{
					cout << "Insira o numero da prova que pretende atualizar: ";
					cin >> codeProva;
				}
			}
			break;
		case '4': //torna ao menu prova
			mProva->menuProva();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcP != 4);
}
//#pragma once

#ifndef MenuConfiguracao_
#define MenuConfiguracao_

#include <iostream>
#include <string>

using namespace std;

class MenuConfiguracao
{
private:
	string sensor;
	int velocidade;
	string funcoes;
	string codigoR;

public:
	MenuConfiguracao();

	void setSensor(string sensores);
	void setVelocidade(int veloc);
	void setFuncoes(string functions);
	void setCodigo(string codigo);

	string getSensor() const;
	int getVelocidade() const;
	string getFuncoes() const;
	string getCodigo() const;

	void informacaoConfiguracao();
};

#endif
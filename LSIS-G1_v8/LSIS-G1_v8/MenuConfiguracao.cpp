#include "MenuConfiguracao.h"
#include <iostream>
#include <string>
#include "BaseDeDados.h"
#include "Menu.h"

//#include "Configuracao.h"
//#include "Robo.h"
//#include "Codigo.h"
//#include "Prova.h"
//#include "Jogador.h"
//#include "Equipa.h"




MenuConfiguracao::MenuConfiguracao()
{

}

void MenuConfiguracao::setSensor(string sensores)
{
	sensor = sensores;
}

void MenuConfiguracao::setVelocidade(int veloc)
{
	velocidade = veloc;
}

void MenuConfiguracao::setFuncoes(string functions)
{
	funcoes = functions;
}

void MenuConfiguracao::setCodigo(string codigo)
{
	codigoR = codigo;
}

string MenuConfiguracao::getSensor() const
{
	return sensor;
}

int MenuConfiguracao::getVelocidade() const
{
	return velocidade;
}

string MenuConfiguracao::getFuncoes() const
{
	return funcoes;
}

string MenuConfiguracao::getCodigo() const
{
	return codigoR;
}


void MenuConfiguracao::informacaoConfiguracao()
{
	char opcC; 
	BaseDados* bd = new BaseDados();
	Menu *mConf = new Menu();
	do
	{
		string codeConfig;
		cout << endl << "***************INFORMACAO DA CONFIGURACAO***************" << endl;
		cout << "1) Listar configuracoes " << endl << "2) Visualizar informacao de uma configuracao" << endl << "3) Atualizar informacao " << endl << "4) Retornar ao menu configuracoes" << endl << "*********************************************" << endl;
		opcC = mConf->lerOpcao();
		switch (opcC)
		{
		case '1': 
			//Listar Configuracoes
			bd->listarConfiguracao();
			break;

		case '2': 
			//Visualizar uma configuracao
			cout << "Introduza o codigo da configuracao que pretende visualizar: " << endl;
			cin >> codeConfig;
			if ((bd->lerConfiguracao(codeConfig)) == true)
			{
				bd->mostrarConfiguracao(codeConfig);
			}
			else
			{
				while ((bd->lerConfiguracao(codeConfig)) == false)
				{
					cout << "Insira o numero da equipa que pretende visualizar: ";
					cin >> codeConfig;
				}
			}
			break;

		case '3': 
			//Atualizar informacao
				  //primeiro e necessario saber qual a configuracao que pretende
			cout << "Introduza o codigo da configuracao que pretende atualizar informacao: " << endl;
			cin >> codeConfig;
			if ((bd->lerConfiguracao(codeConfig)) == true)
			{
				bd->atualizarConfiguracao(codeConfig);
			}
			else
			{
				while ((bd->lerConfiguracao(codeConfig)) == false)
				{
					cout << "Insira o numero da equipa que pretende atualizar: ";
					cin >> codeConfig;
				}
			}
			break;

		case '4': 
			//retorna ao menu configuracoes
			mConf->menuConfiguracao();
			break;
		case '0':
		default:
			cout << "Opcao invalida! Insira novamente a opcao. ";
			break;
		}
	} while (opcC != 4);
}
#ifndef Registo_
#define Registo_

#include <string>

using namespace std;

class Registo
{
	private:
		string nome;
		string pw;
		int id;
	public:
		Registo();
		Registo(const Registo &reg);
		Registo(string name, int iD, string pass);

		void setNome(string name);
		void setPW(string pass);
		void setID(int iD);

		string getNome() const;
		string getPW() const;
		int getID() const;

		void preencherRegisto(int ID);
};

#endif